import {
    Controller,
    Post,
    Body,
    Get,
    Param,
    Patch,
    Delete,
    Put,
    Req,
    Res,
} from '@nestjs/common';

import { UserService } from './user.service'

@Controller('api/user')
export class UserController {
    constructor(private readonly userService: UserService) {

    }
    // _______________________ Register ____________________________
    @Post('/register')
    async createUser(
        @Req() req,
        @Res() res
    ) {
        const generatedId = await this.userService.insertUser(req, res);
        return { id: generatedId, res: res, req: req };
    }

    // _____________________________ Get User ________________________
    @Get('/all')
    async getAllUser() {
        const user = await this.userService.getAllUser();
        return user;
    }
    // ______________________________ Get Single User _____________________________

    @Get(':id')
    getSingleUser(@Param('id') prodId: string) {
        return this.userService.getSingleUser(prodId);
    }
    // ___________________________Update User __________________________

    @Put(':id')
    async updateUser(
        @Param('id') prodId: string,
        @Body('displayName') prodDisplayName: string,
        @Body('phoneNumber') prodPhoneNumber: string,
        @Body('desc') prodDesc: string,
        @Body('photoURL') prodPhotoURL: string,
        @Body('from') prodFrom: string,
        @Body('city') prodCity: string,
        @Body('isAdmin') prodIsAdmin: boolean,
        @Body('coverPhotoURL') prodCoverPhotoURL: string,
    ) {
        await this.userService.updateUser(
            prodId,
            prodDisplayName,
            prodPhoneNumber,
            prodDesc,
            prodCity,
            prodFrom,
            prodIsAdmin,
            prodPhotoURL,
            prodCoverPhotoURL
        );
        return "Complete Updated User";
    }
    // --------------------- Change PassWord ----------------------------



    @Patch('ChangePassword')
    async ChangePassword(
        @Body('newPassword') prodNewPassword: string,
        @Body('oldPassword') prodOldPassword: string,
        @Res() res,
        @Req() req
    ) {
        const user = await this.userService.ChangePassword(
            prodNewPassword,
            prodOldPassword,
            res,
            req,
        )
        return user;
    };

    // ------------------ Login -------------------------------

    @Post("/login")
    async Login(@Req() req, @Res() res) {

        const user = await this.userService.Login(req, res)
        return user;

    }

}
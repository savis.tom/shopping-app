
export interface Comment {
    userId: string;
    displayName: string;
    photoURL?: string;
    userCmt?: string;
}

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CmtProductSchema } from './cmtProduct.model'
import { CmtProductController } from './cmtProduct.controller'
import { CmtProductService } from "./cmtProduct.service"
import { ProductsModule } from '../products/products.module'
@Module({
    imports: [
        ProductsModule,
        MongooseModule.forFeature([{ name: 'CmtProduct', schema: CmtProductSchema }])
    ],
    controllers: [CmtProductController],
    providers: [CmtProductService],
    exports: [CmtProductService]
})
export class CmtProductModule { }

import { Document } from 'mongoose'

export interface IProduct extends Document {
    readonly id?: string;
    readonly name?: string;
    readonly brand?: string;
    readonly descProduct?: string;
    readonly OldPrice?: number;
    readonly NewPrice?: number;
    readonly ColorProduct?: string[];
    readonly Categories?: string[];
    readonly ImgUrlProduct?: object;
    readonly RateProduct?: object[];
    readonly QuantityProductAndSize?: object;
    readonly commentId?: string;
}
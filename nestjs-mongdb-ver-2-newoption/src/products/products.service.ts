import { Test } from '@nestjs/testing';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Product } from './product.model';
import { QueryOptions } from './query-options.config';

import { IProduct } from './interfaces/product.interface'

import { ProductDto } from './dto/product.dto'

import { UpdateDataArray } from './functionProcessData/updateArray'


@Injectable()
export class ProductsService {
  constructor(
    @InjectModel('Product') private readonly productModel: Model<Product>,
  ) { }

  // PostMan

  async insertProduct(
    name: string,
    brand: string,
    descProduct: string,
    OldPrice: number,
    NewPrice: number,
    ColorProduct: object,
    ImgUrlProduct: object,
    RateProduct: object[],
    QuantityProductAndSize: object,
    Categories: string[],
    commentId: string,
  ) {
    const newProduct = new this.productModel({
      name: name,
      brand: brand,
      descProduct: descProduct,
      OldPrice: OldPrice,
      NewPrice: NewPrice,
      ColorProduct: ColorProduct,
      ImgUrlProduct: ImgUrlProduct,
      RateProduct: RateProduct,
      QuantityProductAndSize: QuantityProductAndSize,
      Categories: Categories,
      commentId: commentId,
    });
    const result = await newProduct.save();
    return result.id as string;
  }

  async getProducts(options: QueryOptions, res) {

    // Getting the set Headers
    // const headers = res.getHeaders();

    // // Printing those headers
    // console.log(headers);

    // const totalCountHeader = headers['x-total-count'];


    // console.log(headers, totalCountHeader);

    const _page = options._page ? Number.parseInt(options._page) : 0;
    const _limit = options._limit ? Number.parseInt(options._limit) : 0;
    const _skip = (_page - 1) * _limit;
    const _totalRow = await this.productModel.count();
    // 
    const products = await this.productModel.find().limit(Number(_limit)).skip(_skip).exec();

    const InfoProduct = {
      data: {
        product: products.map(prod => ({
          _id: prod._id,
          name: prod.name,
          brand: prod.brand,
          descProduct: prod.descProduct,
          OldPrice: prod.OldPrice,
          NewPrice: prod.NewPrice,
          ColorProduct: prod.ColorProduct,
          ImgUrlProduct: prod.ImgUrlProduct,
          RateProduct: prod.RateProduct,
          QuantityProductAndSize: prod.QuantityProductAndSize,
          Categories: prod.Categories,
          commentId: prod.commentId,
        })),
        pagination: {
          _page: _page,
          _skip: _skip,
          _limit: _limit,
          _totalRow: _totalRow,
        },
      }
    }

    return res.jsonp(
      InfoProduct
    )
  }

  async getAllProducts() {

    const productRepository = this.productModel;

    const products = await productRepository.find()

    return products.map(prod => ({
      _id: prod._id,
      name: prod.name,
      brand: prod.brand,
      descProduct: prod.descProduct,
      OldPrice: prod.OldPrice,
      NewPrice: prod.NewPrice,
      ColorProduct: prod.ColorProduct,
      ImgUrlProduct: prod.ImgUrlProduct,
      RateProduct: prod.RateProduct,
      QuantityProductAndSize: prod.QuantityProductAndSize,
      Categories: prod.Categories,
      commentId: prod.commentId,
    }));
  }

  async getSingleProduct(productId: string) {
    const product = await this.findProduct(productId);
    return {
      _id: product._id,
      name: product.name,
      brand: product.brand,
      descProduct: product.descProduct,
      OldPrice: product.OldPrice,
      NewPrice: product.NewPrice,
      ColorProduct: product.ColorProduct,
      ImgUrlProduct: product.ImgUrlProduct,
      RateProduct: product.RateProduct,
      QuantityProductAndSize: product.QuantityProductAndSize,
      Categories: product.Categories,
      commentId: product.commentId,
    };
  }

  async updateProduct(
    productId: string,
    name?: string,
    brand?: string,
    descProduct?: string,
    OldPrice?: number,
    NewPrice?: number,
    ColorProductUpdate?: string[],
    ImgUrlProduct?: object,
    RateProduct?: object[],
    QuantityProductAndSize?: any[],
    Categories?: string[],
    commentId?: string,
  ) {
    const product = await this.findProduct(productId);
    const ColorProduct = product.ColorProduct;
    const CategoriesData = product.Categories;
    const updatedProduct = await this.findProduct(productId);
    if (name) {
      updatedProduct.name = name;
    }
    if (brand) {
      updatedProduct.brand = brand;
    }
    if (descProduct) {
      updatedProduct.descProduct = descProduct;
    }
    if (OldPrice) {
      updatedProduct.OldPrice = OldPrice;
    }
    if (NewPrice) {
      updatedProduct.NewPrice = NewPrice;
    }
    if (ColorProductUpdate) {
      const AssignColor = [...ColorProduct, ...ColorProductUpdate];
      const UpdateDataColor = Array.from(new Set(AssignColor));
      // console.log(UpdateDataColor);
      updatedProduct.ColorProduct = UpdateDataColor;
    }

    if (Categories) {
      const AssignCategories = [...CategoriesData, ...Categories];
      const UpdateDataCategories = Array.from(new Set(AssignCategories));
      // console.log(UpdateDataColor);
      updatedProduct.ColorProduct = UpdateDataCategories;
    }

    if (commentId) {
      updatedProduct.commentId = commentId;
    }

    if (ImgUrlProduct) {

      const updateImgUrlProduct = { ...ImgUrlProduct }
      updatedProduct.ImgUrlProduct = updateImgUrlProduct;
    }
    if (RateProduct) {
      updatedProduct.RateProduct = Object.assign(RateProduct);
    }
    if (QuantityProductAndSize) {
      updatedProduct.QuantityProductAndSize = QuantityProductAndSize;
    }

    updatedProduct.save();

  }

  async updateProductCommentId(productId: string, commentId?: string) {
    const updatedProduct = await this.findProduct(productId);
    if (updatedProduct) {
      updatedProduct.commentId = commentId;
    }
    updatedProduct.save();

    return "update done!"
  }

  async deleteProduct(prodId: string, res) {
    const result = await this.productModel.deleteOne({ _id: prodId }).exec();
    // console.log(result.deletedCount)
    if (result.deletedCount === 1) {
      res.status(200).jsonp("Data Product has been deleted");
    }
    if (result.deletedCount === 0) {
      throw new NotFoundException('Could not find product.');
    }
  }

  private async findProduct(id: string): Promise<Product> {
    let product;
    try {
      product = await this.productModel.findById(id).exec();
    } catch (error) {
      throw new NotFoundException('Could not find product.');
    }
    if (!product) {
      throw new NotFoundException('Could not find product.');
    }
    return product;
  }

  // Graphql

  async createProductGraphql(createProductGraphql: ProductDto): Promise<IProduct> {
    const createProduct = new this.productModel(createProductGraphql);
    return await createProduct.save();
  }

  async findAllGraphql(): Promise<IProduct[]> {
    return await this.productModel.find().exec();
  }

  async findByIdGraphql(id: string): Promise<IProduct> {
    return await this.productModel.findById(id).exec();
  }

  async findIdAndUpdate(id: string, updateProduct?: ProductDto): Promise<IProduct> {
    const product = await this.findProduct(id);
    const ColorProduct = product.ColorProduct;
    const Categories = product.Categories;
    const QuantityProductAndSize = product.QuantityProductAndSize;
    const Rate = product.RateProduct

    if (updateProduct.name) {
      product.name = updateProduct.name;
    }

    if (updateProduct.brand) {
      product.brand = updateProduct.brand;
    }

    if (updateProduct.descProduct) {
      product.descProduct = updateProduct.descProduct;
    }

    if (updateProduct.OldPrice) {
      product.OldPrice = updateProduct.OldPrice;
    }

    if (updateProduct.NewPrice) {
      product.NewPrice = updateProduct.NewPrice;
    }

    if (updateProduct.ColorProduct) {
      const AssignColor = [...ColorProduct, ...updateProduct.ColorProduct];
      const UpdateDataColor = Array.from(new Set(AssignColor));
      // console.log(updateProduct.ColorProduct);
      product.ColorProduct = UpdateDataColor;
    }

    if (updateProduct.Categories) {
      const AssignCategories = [...Categories, ...updateProduct.Categories];
      const UpdateDataCategories = Array.from(new Set(AssignCategories));
      // console.log(updateProduct.ColorProduct);
      product.Categories = UpdateDataCategories;
    }

    if (updateProduct.commentId) {
      product.commentId = updateProduct.commentId
    }

    if (updateProduct.ImgUrlProduct) {
      const updateImgUrlProduct = { ...updateProduct.ImgUrlProduct }
      product.ImgUrlProduct = updateImgUrlProduct;
    }

    if (updateProduct.RateProduct) {

      const data = Rate

      let dataValue: any = [];
      const dataArray: any = [];


      for (var i = 0; i < updateProduct.RateProduct.length; i++) {
        dataArray.push({ ...updateProduct.RateProduct[i] })
        dataValue = [...data[Symbol.iterator]()];
      }

      let arr1 = dataValue;
      let arr2 = dataArray;

      // console.log(dataArray)


      const arrUpdate = UpdateDataArray(arr1, arr2, "star");

      // console.log(arrUpdate)

      product.RateProduct = arrUpdate

    }
    if (updateProduct.QuantityProductAndSize) {

      const data = QuantityProductAndSize

      let dataValue: any = []
      const dataArray: any = []

      for (var i = 0; i < updateProduct.QuantityProductAndSize.length; i++) {
        dataArray.push({ ...updateProduct.QuantityProductAndSize[i] })
        dataValue = [...data[Symbol.iterator]()];
      }

      var arr1 = dataValue;
      var arr2 = dataArray;

      const arr = UpdateDataArray(arr1, arr2, "size");

      // console.log(arr);

      product.QuantityProductAndSize = arr

    }
    return product.save();
  }

  async deleteProductGraphql(id: string) {
    const result = await this.productModel.deleteOne({ _id: id }).exec();
    console.log(result)
    if (result) {
      return "Data Product has been deleted";
    }


    if (!result) {
      throw new NotFoundException('Could not find product.');
    }
  }

}

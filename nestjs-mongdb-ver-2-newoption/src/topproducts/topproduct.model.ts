import * as mongoose from 'mongoose';

export const TopProductSchema = new mongoose.Schema({
    name: { type: String, required: true, min: 4, max: 30 },
    brand: { type: String, required: true, min: 4, max: 30 },
    descProduct: { type: String, required: true, max: 500 },
    OldPrice: { type: Number, required: true },
    NewPrice: { type: Number, required: true },
    ColorProduct: { type: Array, default: [] },
    ImgUrlProduct: { type: Object, default: {} },
    RateProduct: { type: Array, default: [] },
    Categories: { type: Array, default: [] },
    QuantityProductAndSize: { type: Array, default: [] },
    commentId: { type: String, default: "" },
});

export interface TopProduct extends mongoose.Document {
    _id: string;
    name: string;
    brand: string;
    descProduct: string;
    OldPrice: number;
    NewPrice: number;
    ColorProduct: string[];
    ImgUrlProduct: object;
    RateProduct: object[];
    Categories: string[];
    QuantityProductAndSize: object[];
    commentId: string;
}

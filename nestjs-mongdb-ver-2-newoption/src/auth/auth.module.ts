import { Module } from '@nestjs/common';
import { UsersModule } from 'src/users/users.module';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport'
import { LocalStrategy } from './local.strategy';
import { SessionSerializer } from './session.serializer';
import { JwtModule } from '@nestjs/jwt'
import { JwtAuthGuard } from './jwt-auth.guard';
import { JwtStrategy } from './jwt.strategy';
import { UserModule } from 'src/user/user.module';
import { AuthController } from './auth.controller';

const fs = require('fs')

let privateKey


try {
  const data = fs.readFileSync('src/user/private.key', 'utf8')
  privateKey = data
  // console.log(data)
} catch (err) {
  console.error(err)
}

// const privateKey = process.env.KEY_PRIVATE;

// console.log(privateKey);

const iss = "https://securetoken.google.com/shopping-app-f29b6"
const sub = "thanhtung19041998@gmail.com"
const aud = "shopping-app-f29b6"
const exp = "1h"

@Module({
  imports: [UsersModule, UserModule, PassportModule, JwtModule.register({
    secret: privateKey,
    signOptions: {
      issuer: iss,
      audience: aud,
      expiresIn: exp,
      keyid: "650ffeefb5f79c3d10af106b9a5daca636cf9774",
      algorithm: "RS256",
    },
  })],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService]
})
export class AuthModule { }

import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service'
const bcrypt = require('bcrypt')


function getTimestamp(date) {
    var tp = Math.round(Date.parse(date) / 1000);
    // console.log(tp)
    return tp;
}



@Injectable()
export class AuthService {
    constructor(private UsersService: UsersService,
        private UserService: UserService,
        private jwtService: JwtService,

    ) { }

    // async validateUser(username: string, password: string): Promise<any> {
    //     const user = await this.UsersService.findOne(username)
    //     if (user && user.password === password) {
    //         const { password, username, ...rest } = user
    //         return rest;
    //     }

    //     return null;
    // }

    async validateUserTest(email: string, password: string): Promise<any> {
        console.log(email);
        const user = await this.UserService.findOneTest(email)
        const validatePassword = await bcrypt.compare(password, user.password)
        if (user && validatePassword) {
            return user;
        }

        return "null";
    }

    async login(user: any, res: any) {
        const payload = {
            displayName: user.displayName,
            subject: user.id,
            email: user.email,
            photoURL: user.photoURL,
            auth_time: getTimestamp(new Date().toLocaleString()),
            email_verified: true,
            user_id: user.id,
        };

        // console.log(user)

        if (user === "null") {
            res.status(401).jsonp("Password or Email Wrong !");
        }
        if (user.email) {
            res.status(200).jsonp(
                {
                    user,
                    access_token: this.jwtService.sign(payload)
                }
            );
        }

    }


}

import { PassportStrategy } from '@nestjs/passport'
import { Injectable } from '@nestjs/common'
import { ExtractJwt, Strategy } from 'passport-jwt'

const fs = require('fs')

let publicKey

const iss = "https://securetoken.google.com/shopping-app-f29b6"
const sub = "thanhtung19041998@gmail.com"
const aud = "shopping-app-f29b6"
const exp = "1h"

try {

    const datapublic = fs.readFileSync('src/user/public.key', 'utf8')
    publicKey = datapublic
    // console.log(data)
} catch (err) {
    console.error(err)
}

export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), // lays token trong request
            ignoreExceptions: false, // bo qua het han token 
            secretOrKey: publicKey, // protect this , move tho env var
            verifyOptions: {
                issuer: iss,
                audience: aud,
                maxAge: exp,
                algorithms: ["RS256"],
            }
        })
    }

    async validate(payload: any) {

        // const user = await this.usersService.getById(payload.sub)
        return { payload }
    }
}
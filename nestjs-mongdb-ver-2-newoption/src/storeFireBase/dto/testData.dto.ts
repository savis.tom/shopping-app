
import { IsString, MinLength } from "class-validator";

export class DataTest {
    @IsString()
    @MinLength(4)
    name: string;

    @IsString()
    @MinLength(4)
    desc: string;
}


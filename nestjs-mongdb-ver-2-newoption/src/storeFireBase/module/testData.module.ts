import { Module } from '@nestjs/common';
import { TestDataController } from '../controller/testData.controller'
import { DataTestService } from '../service/dataTest.service';
import { ConfigModule } from '@nestjs/config';

@Module({
    imports: [ConfigModule.forRoot()],
    controllers: [TestDataController],
    providers: [DataTestService],
    // exports: [DataTestService],  
})
export class TestDataModule { }
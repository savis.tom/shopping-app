- # Why choise TS

Falsy values là các giá trị liệt kê trong bảng dưới.
Ngược lại , tất cả là trythy (kể cả object rỗng, mảng rỗng)

Falsy values là

false , 0 , -0 , 0n(Big Int) , "" '' `` , null , undefined , NaN , document.all


Key of operator 

const Test = keyof Student; ==> laays ra toan bo key kieu union key

/*

JS : - Linh hoạt
     - Không cần phải định nghĩa kiểu dữ liệu hoặc tìm kiểu dữ liệu
     khi sử dụng libs
     - Code nhanh khi làm một mình
     - Cần phải hiểu rõ về JS để tránh những lỗi lặt vặt

TS : - Gặp khó khăn với type system vì bị lỗi liên quan tới kiểu dữ liệu
     - Nhưng được cái nó nhắc code
     - Sau khi team quen dần với typescript thì code sẽ tự tin
       đỡ lỗi vặt hơn đặc biệt là những bạn mới
     - Code một mình thì hơi chậm lúc ban đầu tí, vì phải tìm hiểu kiểu dữ liệu
     khai báo
     - Code cho cả team thì được lợi rất nhiều từ việc auto complete

     --> đi hơi chậm một xíu lúc ban đầu, nhưng lúc sau đi sẽ nhanh và an toàn hơn

*/

/*

Enum: 

What: Tap hop cac gia tri cung nhom 
Why: De dang quan ly va truy xuat
When: Su dung cho data mot chieu, dinh nghia enum --> roi su dung

/*

/*
   Number Enum
   <!-- Default -->
   enum Status {
      PENDING, // 0
      IN_PROGRESS, // 1
      DONE, //2
      CANCELLED, //3
   }

   <!-- Custom -->
   enum Status {
      PENDING = 3, // 3
      IN_PROGRESS, // 4
      DONE, //5
      CANCELLED, //6
   }

*/

/*
   // can assign any number to your enum variable
   const stats1: Status = Status.PENDING;
   const stats2: Status = -1 //oke
   const stats2: Status = -3 //oke
*/

/*

   // number enum --> support reverse mapping 

   console.log(Status[0]); // 'PENDING'
   console.log(Status['DONE']);
 
*/

/*

<!-- String Enum -->

enum MediaTypes {
JSON = "application/json",
XML = "application/xml",
}
fetch("https://example.com/api/endpoint", {
headers: {
Accept: MediaTypes.JSON,
},
}).then(response => {
// ...
});

*/

<!-- Khi nao dung enum -->

Dung cho kieu du lieu dinh nghia ra va dung mot chieu 

Can pass du lieu ra enum ko recomment

/*

Utility Types

1 Partial  Set all props of Type to optional 
2 Required Set all props of Type to required
3 Readonly Set all props of Type to readonly
4 Record<Keys, Type> A type with key from Keys and has value of Type 
5 Pick<Type, Keys> A type by picking a set of Keys from Type
6 Omit<Type, Keys> A type by omitting a set of Keys from Type
7 ReturnType The return type of function Type

*/

```
Doc them
https://www.typescriptlang.org/docs/handbook/utility-types.html
```



```

// Partial Type

interface Todo {
  title: string;
  description: string;
}
 
function updateTodo(todo: Todo, fieldsToUpdate: Partial<Todo>) {
  return { ...todo, ...fieldsToUpdate };
}
 
const todo1 = {
  title: "organize desk",
  description: "clear clutter",
};
 
const todo2 = updateTodo(todo1, {
  description: "throw out trash",
});

```


/*
    Thực thi file TS

    - Compile ra file JS
*/

1. Static type Checking

- TypeScript giúp phát hiện ra lỗi ngay trong lúc code 
- Giúp tránh lỗi typo (phổ biến bên javascript)
- Tiết kiệm thời gian debug

2. Types for Tolling

- Ngoài việc thông báo lỗi cho mình lúc code.
- Typescript còn có thể giúp mình hạn chế lỗi bằng việc hỗ
trợ auto completions / suggestions.(nhắc code , liệt kê phương thức ....) 

Explicit types vs Infered types

1 Explicit types
  - Khai báo kiểu dữ liệu trong typescript thì sủ dụng dấu 2 chấm sau tên biến (Khai báo tường minh)

  const name: string = "Tung"

2. Infered types
- typescript thông minh detect được kiểu dữ liệu tương ứng ngay cả khi mình không khai báo kiểu dữ liệu cụ thể

  const name = "Tung"

- Không phải lúc nào cũng nhận biết đc

3. Eraised types 
  Sau khi compile từ typescript sang js tất cả type annotation sẽ bị xóa

4. Downleveling

- Create__UI--Project
# npx create-react-app my-app --template redux-typescript

# tsdrpfc cu phap viet tat

# tsrpfc

# redux toolkit 

# Css Tailwindcss
-- Set-up 

1. yarn add -D tailwindcss@npm:@tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9

# set up @craco/craco 

-- yarn add @craco/craco

# Change package.json 

 {
    // ...
    "scripts": {
     //"start": "react-scripts start",
     //"build": "react-scripts build",
     //"test": "react-scripts test",
     "start": "craco start",
     "build": "craco build",
     "test": "craco test",
      "eject": "react-scripts eject"
    },
  }

--- create file craco.config.js 

//add

if (a ===)

module.exports = {
  style: {
    postcss: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
      ],
    },
  },
}

# create tailwind.config.js 


# yarn add styled-components twin.macro react-responsive

# Set Up Assets file

# yarn add --dev @types/styled-components

# Navbar 
    - responsive mobile
    - yarn add react-burger-menu
    - create typings react-burger-menu.d.ts

# TopSection 

    - fetch data
    - Css
    - responsive
    - animation


# topProducts
  - install : yarn add @brainhubeu/react-carousel
  - typings create file react-carousel.d.ts and declare module "@brainhubeu/react-carousel"
  - test carousel 
         <Carousel value={current} onChange={setCurrent} slides={[(<Product {...testProduct} />),
                (<Product {...testProduct2} />),
                (<Product {...testProduct} />),
                (<Product {...testProduct} />),]} />
  - Call data từ backend về (axios) Fix bug Carousel
  - Carousel lib bux memory lake ===> đổi sang dùng thư viện khác
  - Xử lý data call về Array.map

# create PolicyCard file
  - Tạo ui
  - Chưa tạo database
  - Chưa FIx bug ui-IP5

# Create List Product  SetUp data fetch ==> {
  data.product   ==> Product Component ==> Chua xu ly Product  /// 5/8/21 --- process Product ====> nth-child()
  data.pagination ==> Pagination Component ==> process Pagination ==> Create Next and Prev
}

# Quick View Product : Xử lý vẫn còn chưa oke nhưng chạy được

==> data product ==> id , data nhỏ hơn ==> thực hiện so sánh id nếu đúng thì nạp data sai thì thôi ==> ........... 
==> CSS QuickView Product ==> Create components SelectLocation

# creat footer fix bug lỗi footer css


// warning do chế độ strict mode (nghiêm ngặt một số components ko ở chết độ này do thư viện sử dụng) ==> Đã Chuyển sang thư viện khác.

/////////////////////////////////////////////////////////////////////////////////
-----------------------------Create Data Base----------------------------------

# Waring CORS : cho phep truy cap khi khac domain port header methods centinal

# Nestjs- using - mysql not support array data ==> dùng mongoose

# Try nestjs using - Mongodb ==> 
set up:  yarn add --save @nestjs/mongoose mongoose 
// set up data Product
// set up data TopProduct ==> fix hợp nhất hai mảng giữ lại value cũ thêm value mới   == Array.from(new Set(...))


# Bug Carousel with TopProduct Data
// vấn đề có thể do database hoặc fe 
// vấn đề là khi loadweb bị lỗi 
// đã bug xem phần ftech data setLoading => // khồng cần đến graphql

# Processed ===>// Data default backend ==> add brand product and top product 

# // Xử lý vấn vấn đê pagination backend ========== getdata 
--yarn add nestjs-seeder --save-dev
creat file product seeder : //failed

# Solution Query
--- Query project use skip and limit  == Check Oke 


--- Bug setActiveModalOpen can not fix with carousel

# Create User and save information User: 

-- Password use Bcrypt Hash + salt mật khẩu  

  salt round normal = 10 (số vòng chạy)

# Working gitlab 

git log --online

git checkout // phuc hoi mot cai gi do vd // git checkout log filecanphuchoi

git switch // dung de chuyen brand

Master ===> merge code dev01 nếu mà lỗi thì xóa đi và merge từ nhành copy 
Dev01 == push code and commit 

// học cách sử dụng formik 


Custom field with formik using FastField


13/8  - Fix Bug Escape AddBasket 
      - Bug Quick View Top Product do Carousel chưa bug được hiện tượng thay đổi đột ngột kích thức bị mất hình tuy nhiên dữ liệu vẫn đc gọi về
      - Đã fix xóa đi infinite
      
14/8  -  Xem qua formik
      -  Tạo database User with nestjs and mongoose

16/8 -- Create User Login/Register Change Password database
     -- SetUp Validations with yup anh formik

17/8 -- Setup ReduxStore User ActionType , Reducer , compile Context ===> Create Provider ===> link no vao trong file index.js để tất cả các components
        có thể sử dụng được redux 
     -- Test Login ===> status 200 (Success)
     

# Which is better? SQL vs NoSQl

 --SQL được định nghĩa khá rõ ràng như một cái gì đó tuân theo cú phap sql
Insert , Update , Select , Delete.... (statements)
Tuân theo một cú pháp sql tiêu chuẩn ==> chúng đếu có cùng chung một cú pháp quy chuẩn ,

 ++Các dữ liệu bên trong được bố trí theo bảng cột , hàng
và dữ liệu ấy được liên kết vói nhau thông qua việc sử dụng các key (key chính , khóa ngoại ) hoặc id nhũng thứ có điểm chung . khả năng mở rộng ko cao

 --NoSQl về cơ bản hoàn toàn ngược lại , chúng không tuân theo quy chuẩn sql , không cần quan tâm đến quan hệ giữa các dữ liệu .

 ++Mọi dữ liệu được lữu trữ dưới dạng văn bản (document) thay vì các bảng các cột . Dữ liệu thường thấy nhất là json . Và các document này không có ý tưởng về khóa ngoại . Tuy nhiên dữ liệu dễ dàng lưu trữ hơn vào trong cơ sở dữ liệu . bạn chỉ cẩn dữ liệu được lưu trữ dữ liệu dưới dạng json và có thể thêm bất cứ thứ gì bên trong đó
Khả năng mở rộng tốt hơn



# // Learn SCSS 

// Variables trong scss sẽ không biên dịch trược tiếp thành biến trong css

khai báo biến : cú pháp $tên_biến
vd : $primary-color: red;
     $Blue-color: blue;

Cách sử dụng 

    body{ 
      background : $primary-color;
    }

Maps in SCSS

$font-weights: (
  "regular": 400,
  "bold": 500,
)

cách sử dụng 

body{ 
      background : $primary-color;
      // cú pháp font-weight: map-get($map , $key)
      font-weight: map-get($font-weights,bold)
    }

Lồng nhau giới hạn phạm vi ví dụ
```
Vi du 1
    .main { 
      width: 100%;
      margin 0 auto;
        p{
          color: red;
          ....
        }
    }
```

```
Vi du 2

  .main { 
      width: 100%;
      margin 0 auto;
        .main-paragraph {
          color: red;
          ....
        }
    }

    sort-cut

     .main { 
      width: 100%;
      margin 0 auto;
      // & == .main
        &__paragraph {
          color: red;
          ....
        }
    }

    kieu nay khi compile se ra .main__paragraph
```

```

vi dụ 3

      fix kieu .main .main__paragraph
      .main { 
      width: 100%;
      margin 0 auto;
      // #{&} == .main .main__paragraph
        #{&}__paragraph {
          color: red;
          ....
        }
       //  & == .main .main__paragraph
        &:hover:{
          ....
        }
    }
```    

vi du muon compile tat ca cac scss vao 1 file
trinh bien dich se bo qua cac file co dau _ ở đầu 
sau đó ta import file do vào file chính sẽ đạt được hiệu quả gộp file

ví dụ 1 file header được viết riêng và bạn không muốn file đấy được compile ra
hãy đặt tên cho nó _header.scss sau đó import vào file gốc là được

///
function trong sass giống với function trong javascript
```

@function weight($weight-name){
  return map-get($font-weights , $weight-name)
}

.main{
  font-weight:weight(bold)
}

ví dụ không muốn viết lại nhiều lần

display: flex;
align-items: center;
justify-content: center;

===> dùng mixin 

@mixin flexCenter($direction) {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: $direction;
}

cách dùng
.main {
  @include flexCenter(column);
}

thích hợp làm cách theme 

@mixin theme($light-theme: true) {
    @if $light-theme {
      background: lighten($primary-color, 100%); 
    }else {
       background: lighten($blue-color, 100%); 
    }
}

.light{
  include theme($light-theme: true);
}

làm mobile

$mobile : 800px;

@mixin mobile {
  @media (max-width:$mobile){
    @content;
  }
}

.main {
  @include flexCenter(column);

   @include mobile {
     flex-direction: row;
   };
}

kế thừa extends

     .main { 
      width: 100%;
      margin 0 auto;
      // #{&} == .main .main__paragraph
        #{&}__paragraph1 {
          color: red;
          ....
        }
       //  & == .main .main__paragraph
        &:hover:{
           color: blue;
        }

      #{&}__paragraph2{
       @extend .main__paragraph1
  // kết thừa từ thanng main__paragraph1
        :hover{
          color: green;
        }
      }

    }
////////////////// Target (>)

  &.active > a {
    color: pink;
  }

```

# #####################################################

# // Learn Redux 

1. Redux là gì ? Kiến trúc của nó ra sao

- Redux is a predictable state container for javascript apps
(là một thư viện js giúp quản lý state mà có thể dự đoán được)
Sử dụng kiến trúc uni-directional data flow (dữ liệu 1 chiều)


[Store] ---> [View] ---> [Action] ---> [Store]

Có dữ liệu gì --> view hiển thị và khi view có 1 hành động-->
action (js object) ---> Store(cập nhật lại store) --> cập nhật lại view


# Trong Store có:

Dispatcher: Quản lý middleware truyền thông tin --> Api 
            
Reducer(stata cũ, và action) ==> state mới ==> render view
Chính nhờ Reducer --> redux thành predictable
Cùng một cái state cùng một action thì nó sẽ luôn cho ra cái mới giống nhau

State(Mới) ==> cập nhật UI

2. Khi nào cần sử dụng Redux?

Dữ liệu được sử dụng ở nhiều nơi
Có hỗ trợ chức năng undo/redo
Cần cache dữ liệu để tái sử dụng nhiều lần sau

# VD : 

import { createStore } from 'redux'

// Step 1: Define a reducer
// A pure js function
// that transform the old state to the new one
// based on the action.type

function counter(state = 0, action) { //nhan vao 2 tham so 1 state 2 là action(js object)
switch (action.type) {
case 'INCREMENT':
return state + 1
case 'DECREMENT':
return state - 1
default:
return state
}
}

//Chuyển đổi state cũ thành state mới dựa trên action type


// Step 2: Init your store with the reducer
// Its API is { subscribe, dispatch, getState }.
let store = createStore(counter)


// Step 3: Subscribe to state changes to update UI
store.subscribe(() => console.log(store.getState()))


// Step 4: Dispatch action to update redux state
// The only way to mutate the internal state is to dispatch an action.
store.dispatch({ type: 'INCREMENT' }) // 1
store.dispatch({ type: 'INCREMENT' }) // 2
store.dispatch({ type: 'DECREMENT' }) // 1

 
// Quy trình làm việc với redux
 
// Cài đặt package redux and react-redux

yarn add --save redux react-redux

# Tổ chức folder (chia theo loại)
```

src
|__ redux
|    |__reducers
|       |__ hobby.js
|       |__ todo.js
|       |__ ......
|       |__ index.js(Tổng hợp các reducers nhỏ vào 1 reducers chính)
|    |__actions
|       |__ hobby.js
|       |__ todo.js
|       |__ index.js(Tổng hợp các actions)
|
|
|
|__ store.js (tổng hợp reducers và actions vào)
|__ index.js (set up Store Provider để các components có thể join chung và 1 redux)

```
********************************

# Connect redux

Connect vào redux từ reactjs component 

-- Với Class component: dùng HOC connect()
-- với Functional component: dùng hooks useSelector() và useDispatch()
-- component: có thể dùng nhiều useSelector() để lấy ra các state tương ứng
-- cho 1 function để dispatch action : useDispatch()

*********************************

# Làm việc với Redux thông qua hooks

- useSelector()
- useDispatch()

1. Setup redux provider

- Allow redux store to be accessible from anywhere of the app.

3. Connect to redux store from component

- Using the two hook 

# UseSelector

Sử dụng strict comparison === mỗi lần redux store thay đổi component sẽ bị rerender lại nếu có sự thay đổi của state.

recommend tách useSelector

 --Vd:
      const HobbyList = useSelector (state => state.hobby.list);
      const activeId = useSelector(state => state.hobby.active);


không phải Shallow comparision : {a , b} so sanh {a , b} so sánh key
```
  --không nên sử dụng kiểu này vì mỗi khi store thay đổi các
  các commponent ko có sự thay đổi cũng bị rerender lại

    const hoobyState = useSelector(state => ({
        list: state.hobby.list,
        activeId: state.hobby.active,
    }))
```
    nếu vẫn muốn gộp lại thành 1 object thì giải pháp là sử dụng shallowEqual của react-redux
      
```
      const hoobyState = useSelector(state => ({
        list: state.hobby.list,
        activeId: state.hobby.active,
      }),shallowEqual)

```

# Tổng quan về Redux-Toolkit

- Là thư viện giúp quản lý Redux tốt hơn 
Dễ hơn và đơn giản hơn (Tiêu chuẩn để viết Redux)

Ba vấn đề nền tảng ra đời ReduxToolKit(RTK):

1. Những hàm cơ bản.

configureStore()
- Có sẵn Redux DevTools
- Có sẵn redux-thunk để thực hiện async actions
```
<!-- Khi chưa có Redux ToolKit -->
<!-- store.js -->

import { createStore, applyMiddleware, compose } from'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers';

// Enable to use redux dev tool in development mode
const composeEnhancers = 'development' === process.env.NODE_ENV
? (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose)
: compose;

// Use redux-thunk as a redux middleware
const enhancer = composeEnhancers(applyMiddleware(thunkMiddleware));

const store = createStore(rootReducer, {}, enhancer);
export default store;
```

```
<!-- Khi đã có redux toolkit -->
<!-- store.js -->


import {configureStore} from '@redux/tookit'
import rootReducer from './reducers'

const store = configureStore({reducer: rootReducer})

createReducer()
```

```

<!-- Không có Redux ToolKit -->

function counterReducer(state = 0, action) {
switch (action.type) {
case 'increment':
  return state + action.payload
case 'decrement':
  return state - action.payload
default:
return state
}
}

```

// Có Redux Toolkit
// - Mỗi key là một case
// - Không cần handle default case

```
const counterReducer = createReducer(0, {
  increment: (state, action) => state + action.payload,
  decrement: (state, action) => state - action.payload
})

// Một điểm hay nữa là reducer có thể mutate data trực tiếp.
// Bản chất bên dưới họ sử dụng thư viện Immerjs

const todoReducer = createReducer([], {
addTodo: (state, action) => {

<!-- 1. Có thể mutate data trực tiếp 🎉 -->
<!-- Trước đây phải clone trước khi cho ra dữ liệu mới tránh việc mutate data-->

state.push(action.payload)
},

removeTodo: (state, action) => {

<!--  2. Hoặc phải trả về state mới -->
<!--  CHỨ KO ĐƯỢC cả 1 và 2 nha 😎 -->

const newState = [...state];
newState.splice(action.payload, 1);
return newState;
}
})

```


--- createAction()

```

// Không có redux toolkit
const INCREMENT = 'counter/increment'
function increment(amount) {
return {
type: INCREMENT,
payload: amount
}
}
const action = increment(3)
// { type: 'counter/increment', payload: 3 }

// Có redux toolkit

const increment = createAction('counter/increment')

const action = increment(3)
// returns { type: 'counter/increment', payload: 3 }

console.log(increment.toString())
// 'counter/increment'

```

# Đọc thêm: 

-- createSlice(): https://redux-toolkit.js.org/api/createSlice (Recommend working)
createSelector(): https://redux-toolkit.js.org/api/createSelector
createAsyncThunk(): https://redux-toolkit.js.org/api/createAsyncThunk
createEntityAdapter(): https://redux-toolkit.js.org/api/createEntityAdapter


# createSlice() với Slice này trước kia phải khai báo tách riêng Reducer và actionType giờ thì nhờ có Slice có thể gộp chung nó vào 1 file createSlice() sẽ tự động tách ra ở Behind

# createAsyncThunk

```

export const getUser = createAsyncThunk('user/getUser', async(params, thunkApi) =>{
  //thunkApi.dispatch(....)
  const currentUser = await userApi.getUser();
  return currentUser;
} )

```

# Redux Saga

Bước 1: Learn Generator Function 
```

      - Generator Function là gì : generator là một object được trả về từ 1 gennerator function
      - Generator ko thể được tạo ra trực tiếp

      cú pháp function* genneraterId(){
        yield: 1;

        console.log("....")

        yield: 2;

      }

      const newId =  genneraterId();

      newId.next();
      newId.next();
      newId.next();

```

      Đặc điểm của gennerator function:

      là hàm có thể dừng giữa chừng và tiếp tục sau

      mỗi lần gọi , hàm sẽ được thực thi cho đến khi gặp lệnh yield tiếp theo hoặc return 


1. Effect (của Saga) : đơn giản là 1 js object chứa thông tin để cho redux saga biết nó phải làm gì
vd : call(myFun, 'arg1', 'arg2') ==> middleware saga ==> behind myFun('agr1','arg2')

2. Task : là một cái process chạy dưới background tương tự như Promise

3. Blocking/non Blocking call : Phải đợi nó thực thi xong mới chạy sang lệnh tiếp theo

4. Watcher / Worker : theo doi va lam viec 



# Tạo ansyn action trong redux Toolkit

Mong muốn:

Lấy thêm thông tin của user sau khi có token 
Thông tin user sẽ được lưu trong Redux

--> dùng async action 

Tổ chức forder Api 

Tất cả các http Request sẽ đi qua API Client

# Auth/ authentication
- login
-sign up / register
-forget password

Click Login
- Call Api to Login
- Succes --> redirect user
- Faled --> show Error


# authSaga

- if logged in , watch logout
- else watch login

Login

- Call login APi to get token + user info
- set token to localstorage
- redirect page

Logout clear token from local storage
- redirect to login page Sử lý bằng React-Router-DOM


# 26/8 Fix CORS; Create ENV ; View Tutorial ; Done Login with redux saga 

# Generator function.

Là các hàm đặc biệt cho phép tạm ngưng việc thực thi hàm , sau đó có thể quay lại thực thi tiếp

Có 3 methods: 
next() => Tiếp tục thực thi cho đến khi gặp lệnh yield hoặc return.

return() => Dừng generator function và trả về kết quả

throw() => Dừng generator function và trả về lỗi 


```

Ví dụ function* generatorId(){
  console.log("start")
  yield 1;

  console.log('continue to run');
  yield 2;

  console.log('Resum');
  return 3;

}

const newId = generatorId();

newId.next(); // {value: 1 , done ; false}
newId.next(); // {value: 2 , done ; false}
newId.next(); // {value: 3 , done ; true}
newId.next(); // {}

```


# Một số Effect creator nên biết trong Redux Saga
```
1. Phân biệt Effect vs Effect Creator

Effect: chỉ đơn giản là một js object có chứa thông tin để Saga Middleware biết cần phải làm cái gì.

Effect Creator: là function trả về một Effect. Và nó không thực thi effect, người thực thi là saga middleware.

Các hàm hay dùng trong Redux Saga như takeEvery, takeLatest, đấy là các Effect Creator

```

```
2 Các Effect Creator nên biết

takeEvery(pattern,saga, ...args) Chạy saga mỗi lần có action pattern được dispatch, dispatch bao nhiêu sẽ chạy bấy nhiêu cái saga

takeLatest(pattern,saga, ...args) Chạy saga, nhưng khi có action pattern mới được dispatch , thì cái saga trước đó sẽ bị cancel và lấy thằng mới nhất.

takeLeading(pattern,saga, ...args) chạy saga khi action pattern được dispatch , những action tiếp theo sẽ bị cancel cho đến khi saga trước đó chạy xong.

put(action) Dispatch action từ saga. Đang ở saga muốn put một cái action lên store thì có thể dùng put

call(fn, ...args)  gọi hàm fn truyền tham số args vào hàm đó

all([...effects]) chạy tất cả effect cùng một lúc

take(pattern) and fork(fn,...args) Mô hình watcher ...worker , đợi dispatch action pattern thì sẽ thực hiện một task nào đó

throttle(ms,pattern,saga, ...args) Throttle cái action pattern, đảm bảo chỉ chạy saga một lần trong khoảng thời gian ms

debounce(ms,pattern,saga, ...args) Debounce cái action pattern,đảm bảo chỉ chạy saga 1 lần sau khi đã đợi khoảng thời gian ms

retry(maxTries,delay,fn,...args) Cố gắng gọi lại hàm fn nếu faield sau mỗi delay milliseconds và tối đa chỉ thử maxTries lần

```

Đọc thêm tại https://redux-saga.js.org/docs/api/#effect-creators


# So sánh giữa takeLatest và takeEvery

-- takeLatest Chỉ chạy cái pattern cuối cùng và cancel những cái trước đó cho dù đã chạy xong hay chưa chạy xong.

-- takeEvery chạy tất cả các pattern được gửi lên

# Tìm hiểu thêm về call

1. Tác dụng
-- Gọi API và truyền vào tham số --> trả về js object
-- Dễ viết Test case hơn

# handle navigation trong redux saga

-- 1 Watch redux store and make redirect on component

-- Được nhưng rối.
-- Flow is fragmented, hard to control when you have more and more state

-- 2 use Callbacks

the approach using non-serializable (callback) in action and dispatch to redux store which is NOT RECOMMENDED by redux toolkit
  
  ```

  const function App(){
    const dispatch = useAppDispatch();

    const handleLoginSubmit = (values) => {
      dispatch(authActions.login({
        ...values,
        onSuccess: () => history.push('/admin'),
        onError: () => console.log("Login failed")
      }))
    }
  }

  ```

3. Using connected-react-router

. Sync routings to redux.
. mavigate by dispatching an action to redux store
. Onething to make sure, when route changes, it doesn't cause re-reder our components

--> We'll go with this solution for now

Lib: connected-react-router + custom history

// Process productDesc data 

Solution 1: Css white-space: pre-wrap; -- Khoảng trắng sẽ do trình duyệt điều khiển, văn bản sẽ tự động bao lại khi cần thiết, và xuống hàng.

process after send data to server
```
  var InputElement = document.querySelector('#ta');
    InputElement.oninput = function (e) {
        let value = e.target.value;
        var s = value.replace(/\n/gi, '\\n');
        //Công cụ sửa đổi g được sử dụng để thực hiện đối sánh toàn cục (tìm tất cả các kết quả phù hợp hơn là dừng lại sau trận đấu đầu tiên).
        console.log(s)
    }
```
# Navigator In Nextjs
```
  chú ý: next js sử dụng một thư việc router riêng có tên next/router
  -- ko cần phải router như router dom mỗi file trong cây thư mục page đợi diện cho một router riêng
  -- ví dụ
  -- page |
          | - index.js    ==> navigator localhost:3000
          | - Details.js  ==> navigator localhost:3000/details
          | product |
          |         |     ==> product.js localhost:3000/product
          |         |     ==> [id].js    localhost:3000/product/:id
                          lưu ý nếu để tên file trong dau [] thi đường dẫn ko cần chính xác tuyệt đối

  -- muốn đọc thông tin trên url : sử dụng thư viên next/router
    import {useRouter} from 'next/router'
  -- Link để di chuyển giữa các trang href={`/product/${id}`}
```


  # JWT : Json Web Token 

  -- Mục đích sử dụng của JWT là : Sử dụng để Xử lý xác thực và ủy quyền
  Ví dụ ứng dụng của bạn , người dùng có thể tạo bài post chỉnh sủa , xóa . 
  Khi mà người dùng khác thực hiện chức năng tương tự trên bài viết đó họ sẽ nhận đc thông báo bài viết đó không phải bài viết của họ
  
  Giả dụ bạn đăng nhập với user và pass word thông thường ==> điều này thiếu sự an toàn .

  Điều này rất nguy hiểm với cái giao dịch mà không check dữ liệu rõ ràng

  Hacker có thể thay đổi thông tin được lưu trong localstorage để tấn công vào mạng lưới

  Vấn đề do Api ==> Solution là gì ?

  Giải pháp ở đây khi người dùng đăng nhập bằng thông tin đăng nhập của mình, nó sẽ truy cập vào api và nếu thông tin chính xác ==> sẽ tạo ra một localstorage , cookie bên trong máy chủ của bạn

  Khi người dùng thực hiện một chức năng gì đó cookie này được giử lên server để xác thực 

  Giải pháp ở đây là Sử dụng JWT khi người dùng đăng nhập một JWT được tạo ra đây là mã thông báo bí mật không ai có thể tạo và giử cho bạn mà không lưu trữ nó ở bất kỳ đâu

  Khi bạn cố gắng xóa tài khoản của mình bạn sẽ phải cung cập kèm theo mã thông báo đó trong yêu cầu truy cập của bạn , máy chủ sẽ giải mã nó và check với dữ liệu trong máy chủ rồi đưa ra quyêt định quyền hạn của bạn

  JWT bao gồm 3 phần : header , signature , Payload

  Giả sử bạn không chỉ có một server : Server lưu trữ thông tin riêng , server lưu trữ thông tin người dùng riêng , server kết nối khác hàng riêng . Bạn có thể sử dụng JWT để chia sẽ thông tin giữa các server


Ví dụ sử dụng JWT 
```
npm install jsonwebtoken

const express = require("express")
const app = express();
const jwt = require("jsonwebtoken")

app.user(express.json());
<!-- cho phép truyền dữ liệu dưới dạng json và đọc nó -->

const users = [
  {
    id: "1",
    username: "Tung",
    password: "123456"
    isAdmin: true
  },
  {
    id: "2",
    username: "Tung1",
    password: "123456"
    isAdmin: fasle
  }

]

let refreshToken = []

  const generateAccessToken = (user) => {
  return jwt.sign({id: user.id, isAdmin: user.isAdmin, ...},"mySecretKey",{expiresIn"4d"});
  }

  const generateRefreshToken = (user) => {
  return jwt.sign({id: user.id, isAdmin: user.isAdmin, ...},"myRefreshSecretKey");
  }


app.post("/api/login",(req,res))=>{
  const { username , password } = req.body;

  const user = users.find(u => u.username === username && u.password === password);

  if(user){
    <!-- Generate an access token  -->
    const accessToken = generateAccessToken(user)
    const refreshToken = generateRefreshToken(user)

    refreshToken.push(refreshToken);

    <!-- expiresIn thời gian tồn tại của cái acsess token 4 day -->

    res.json({user , accessToken})
  }else{
    res.status(400).json("Username and Password incorrect!")
  }
}

const verify = (req,res,next) => {
  const authHeader = req.header.authencation;
  <!-- Bearer acsessToken -->

  if(authHeader){
    <!-- ...some thing hear -->
    const token = authHeader.split("")[1];
    jwt.verify(token,"mySecretKey",(err,user)=>{
      if(err){
        return res.status(401).json("Token is not Valid");
      }
      req.user = user
      next();
    
    });
  }else{
    res.status(400).json("You are not authentication")
  }
}



app.post("/api/refresh", (req,res)=>{
  <!-- take the refresh token from the user -->

  const refreshtoken = req.body.token

  <!-- send error if there is no token or it's invalid -->
  if(!refreshtoken) return res.status(401).json("you are not authenticated!")

  if(!refreshToken.includes(refreshToken)){
    return res.status(403).json("Refresh token is not valid!")
  }
  jwt.verify(refreshToken,"myRefreshSecretKey",(err,user)=>{
    err && console.log(err);
      refreshToken = refreshToken.filter((token)=> token !== refreshToken);

  const NewAccessToken = generateAccessToken(user)
  const NewRefreshToken = generateRefreshToken(user)
  
  const refreshToken.push(NewRefreshToken);

  res.status(200).json()
    accessToken: NewAccessToken,refreshToken: NewRefreshToken
  })
  <!-- if everything is oke , create new access token refresh token and send to user -->
})

app.delete("/api/users/:userId", verify, (req,res)=>{
    if(req.user.id === req.params.userId || req.user.isAdmin){
      res.status(200).json("user has been delete")
    }else{
      res.status(400).json("Not delete")
    }
})

app.post("/api/logout",verify,(req,res)=>{
  const refreshToken = req.body.token;
  refreshTokens = refreshToken.filter(token => token !== refreshToken);
  res.status(200).json("You logged out successfully")
})

}

app.listen(5000, () => console.log("Backend server is running!"));

```

Authentication(Bạn là ai) VS Authorization(Bạn có những quyền gì)

Cơ chế Authentication

Xác định bạn có phải là user của hệ thông ko ? Nếu không phải thì bạn là ai ?

Có thể sử dụng một hoặc nhiều phương thức khác nhau

- Username + password
- Vân tay
- Nhận diện khuôn mặt
- Social network
- OTP
- PIN
- Two-factor authentication(2FA)
- Multi-factor authentication(MFA)
- ......

Cơ chế Authorization

Nếu đã là user của hệ thống , vậy bạn có quyền gì trên hệ thống 

Một trong những cách phổ biến nhất hiện nay là sử dụng JWT (Json web token)

Authenticaiton sảy ra trước thằng authorization

Authentication nếu lỗi trả về 401
authorization nếu lỗi trả về 403

cookie Based Auth  VS Token Base Auth

Với cookie Base
```
Browser               Server
  |  Post/Authenticate   |
  |--------------------->|
  |                      |
  |<---------------------|
  |  Set-Cookie: session | 
  |                      | 
  |    Get /api/user     |
  |    Cookie: session   |           
  |--------------------->| "
  |                      | " find and deserialize session
  |<---------------------| "
        Http: 200 OK

Server lưu thông tin cookie của người dùng để lần sau có thể so sánh
==> Server phải làm nhiều việc
```

Token Base
```

Browser               Server
  |  Post/Authenticate   |
  |--------------------->|
  |                      |
  |<---------------------|
  |     Token: (JWT)     | 
  |                      | 
  |    Get /api/user     |
  |     Token: (JWT)     |           
  |--------------------->| "
  |                      | " Validate Token
  |<---------------------| "
        Http: 200 OK
```

Token: Được lưu trong localstorage 
khi gửi lên server sẽ được truyền vào header 
Authorization: Bearer JWT
Trên server sẽ validate Token 
Kiểm tra token xem có hợp lệ ko ==> Trả về dữ liệu tương ứng

```

            |     Cookie       |   Token(Recommended)

  State       Stateful Server       Stateless Server
  Scale       Hard to achieve       Easy to scale
Controlled by      Server                Client
Cross domain       NO                     Yes
Size               Tiny                   Large

```

JWT With RS256 As the same fire base

public key view "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-mzlu1%40shopping-app-f29b6.iam.gserviceaccount.com"
as the same private key using from fire base

-- exp       :	Thời gian hết hạn	Phải có trong tương lai. Thời gian được tính bằng giây kể từ kỷ nguyên UNIX.
-- iat	      : Được cấp-tại thời điểm	Phải trong quá khứ. Thời gian được tính bằng giây kể từ kỷ nguyên UNIX.
-- aud	      : Thính giả	Phải là ID dự án Firebase của bạn, mã định danh duy nhất cho dự án Firebase của bạn, có thể được tìm thấy trong URL của bảng điều khiển của dự án đó.
-- iss       :	Người phát hành	Phải là "https://securetoken.google.com/<projectId>" , nơi <projectId> là cùng một ID dự án sử dụng cho aud trên.
-- sub	      : Chủ thể	Phải là một chuỗi không trống và phải là uid của người dùng hoặc thiết bị.
-- auth_time :	Thời gian xác thực	Phải trong quá khứ. Thời điểm người dùng xác thực.

# Data Structure and Algorithms

-- Cấu trúc dữ liệu là một cách lưu trữ, tổ chức dữ liệu có thứ tự , có thể thống để dữ liệu có thể sử dụng một cách hiệu quả.

-- Interface(giao diện): Mỗi cấu trúc dữ liệu có một interface. Interface biểu diễn một tập hợp các phép tính mà một cấu trúc dữ liệu hỗ trợ. Một interface chỉ cung cấp danh sánh các phép tính được hỗ trợ, các loại tham số mà chúng có thể chấp nhận và kiểu trả về của các phép tính này.

-- Implementation (có thể hiểu là sự triển khai): Cung cấp sự biểu diễn nội bộ của một cấu trúc dữ liệu. Implementation cũng cung cấp phần định nghĩa của giải thuật được sử dụng trong các phép tính của cấu trúc dữ liệu.

# -- # đặc điểm của một cấu trúc dữ liệu

Chính xác: Sự triển khai của cấu trúc dữ liệu nên triển khai interface của nó một cách chính xác.

Độ phức tạp về thời gian (Time complexity): Thời gian chạy hoặc thời gian thực thi của các phép tính của cấu trúc dữ liệu phải là nhỏ nhất có thể.

Độ phức tạp về bộ nhớ (Space Complexity): Sự sử dụng bộ nhớ của mỗi phép tính của cấu trúc dữ liệu lên là nhỏ nhất có thể.

# -- # tại sao cấu trúc dữ liệu là cần thiết ?

Vấn đề thường gặp phải là : 

+ Tìm kiếm dữ liệu: Khi dữ liệu tăng lên thì việc tìm kiếm sẽ càng trở lên chậm và tốn kém hơn.

Tốc độ bộ vi xử lý: Mặc dù bộ vi xử lý có tốc độ rất cao, tuy nhiên nó cũng có giới hạn và khi lượng dữ liệu lên tới hàng tỉ bản ghi thì tốc độ xử lý cũng sẽ không còn được nhanh nữa.

Đa yêu cầu: Khi hàng nghìn người dùng cùng thực hiện một phép tính tìm kiếm trên một WebSever thì dùng WebSever đó có nhanh đến mấy thì việc xử lý hàng nghìn phép tính cùng một lúc là thực sự rất khó.

# -- # Độ phức tạp thời gian thực thi trong cấu trúc dữ liệu và giải thuật.

+ Trường hợp xấu nhất (Worst Case): là tình huống mà một phép tính của cấu trúc dữ liệu nào đó tốn thời gian tối đa (thời gian dài nhất)

+ Trường hợp trung bình (Average Case): Miêu tả thời gian thực thi trung bình một phép tính của một cấu trúc dữ liệu 

+ Trường hợp tốt nhất (Best Case): là tình huống mà thời gian thực thi một phép tính của một cấu trúc dữ liệu là ít nhất 

# -- # Thuật ngữ cơ bản trong cấu trúc dữ liệu 

+ Dữ liệu: Dữ liệu là các giá trị hoặc là tập hợp các giá trị.

+ Phần tử dữ liệu: Phần tử dữ liệu là một đơn vị lẻ của giá trị.

+ Các phần tử nhóm: Phần tử dữ liệu mà được chi thành các phần tử con thì được gọi là các phần tử nhóm.

+ Các phần tử cơ bản: Phần tử dữ liệu mà được chia thành các phần tử con thì dược gọi là các phần tử nhóm.

+ Các phần tử cơ bản: Phần tử dữ liệu mà không thể bị chia nhỏ thành các phần tử con thì gọi là các phần tử cơ bản.

+ Thuộc tính và Thực thể: Một thực thể là cái mà chứa một vài thuộc tính nào đó, và các thuộc tính này có thể được gán các giá trị.

+ Tập hợp thực thể: Các thực thể mà có các thuộc tính tương tự nhau thì cấu thành một tập hợp thực thể.

+ Trường: Trường là một đơn vị thông tin cơ bản biểu diễn một thuộc tính của một thực thể.

+ Bản ghi: Bản ghi là một tập hợp các giá trị trường của một thực thể đã cho.

+ File: Là một tập hợp các bản ghi của các thực thể trong một tập hợp thực thể đã cho.

# -- # ❤️ Giải thuật

# ✔️ Giải thuật là gì ? 

   + Giải thuật (hay còn gọi là thuật toán - tiếng Anh là Algorithms) là một tập hợp hữu hạn các chỉ thị để được thực thi theo một thứ tự nào đó để thu được kết quả mong muốn. Nói chung thì giải thuật là độc lập với các ngôn ngữ lập trình, tức là một giải thuật có thể được triển khai trong nhiều ngôn ngữ lập trình khác nhau.

  -- Xuất phát từ quan điểm của cấu trúc dữ liệu, dưới đây là một số giải thuật quan trọng:

    + Giải thuật Tìm kiếm: Giải thuật để tìm kiếm một phần tử trong một cấu trúc dữ liệu.

    + Giải thuật Sắp xếp: Giải thuật để sắp xếp các phần tử theo thứ tự nào đó.

    + Giải thuật Chèn: Giải thuật để chèn phần từ vào trong một cấu trúc dữ liệu.

    + Giải thuật Cập nhật: Giải thuật để cập nhật (hay update) một phần tử đã tồn tại trong một cấu trúc dữ liệu.

    + Giải thuật Xóa: Giải thuật để xóa một phần tử đang tồn tại từ một cấu trúc dữ liệu.

# ✔️ Đặc điểm của giải thuật

  --Không phải tất cả các thủ tục có thể được gọi là một giải thuật. Một giải thuật nên có các đặc điểm sau:

    + Tính xác định: Giải thuật nên rõ ràng và không mơ hồ. Mỗi một giai đoạn (hay mỗi bước) nên rõ ràng và chỉ mang một mục đích nhất định.

    + Dữ liệu đầu vào xác định: Một giải thuật nên có 0 hoặc nhiều hơn dữ liệu đầu vào đã xác định.

    + Kết quả đầu ra: Một giải thuật nên có một hoặc nhiều dữ liệu đầu ra đã xác định, và nên kết nối với kiểu kết quả bạn mong muốn.

    + Tính dừng: Các giải thuật phải kết thúc sau một số hữu hạn các bước.

    + Tính hiệu quả: Một giải thuật nên là có thể thi hành được với các nguồn có sẵn, tức là có khả năng giải quyết hiệu quả vấn đề trong điều kiện thời gian và tài nguyên cho phép.

    + Tính phổ biến: Một giải thuật có tính phổ biến nếu giải thuật này có thể giải quyết được một lớp các vấn đề tương tự.

    + Độc lập: Một giải thuật nên có các chỉ thị độc lập với bất kỳ phần code lập trình nào.

# ✔️ Cách viết một giải thuật ?

    Bạn đừng tìm, bởi vì sẽ không có bất kỳ tiêu chuẩn nào cho trước để viết các giải thuật. Như bạn đã biết, các ngôn ngữ lập trình đều có các vòng lặp (do, for, while) và các lệnh điều khiển luồng (if-else), … Bạn có thể sử dụng những lệnh này để viết một giải thuật.

    Chúng ta viết các giải thuật theo cách thức là theo từng bước một. Viết giải thuật là một tiến trình và được thực thi sau khi bạn đã định vị rõ ràng vấn đề cần giải quyết. Từ việc định vị vấn đề, chúng ta sẽ thiết kế ra giải pháp để giải quyết vấn đề đó và sau đó là viết giải thuật.

# ✔️ Phân tích giải thuật

  --Hiệu quả của một giải thuật có thể được phân tích dựa trên 2 góc độ: trước khi triển khai và sau khi triển khai:

    + Phân tích lý thuyết: Có thể coi đây là phân tích chỉ dựa trên lý thuyết. Hiệu quả của giải thuật được đánh giá bằng việc giả sử rằng tất cả các yếu tố khác (ví dụ: tốc độ vi xử lý, …) là hằng số và không ảnh hưởng tới sự triển khai giải thuật.

    + Phân tích tiệm cận: Việc phân tích giải thuật này được tiến hành sau khi đã tiến hành trên một ngôn ngữ lập trình nào đó. Sau khi chạy và kiểm tra đo lường các thông số liên quan thì hiệu quả của giải thuật dựa trên các thông số như thời gian chạy, thời gian thực thi, lượng bộ nhớ cần dùng, …

# ✔️ Độ phức tạp giải thuật (Algorithm Complexity)

  -- Về bản chất, độ phức tạp giải thuật là một hàm ước lượng (có thể không chính xác) số phép tính mà giải thuật cần thực hiện (từ đó dễ dàng suy ra thời gian thực hiện của giải thuật) đối với bộ dữ liệu đầu vào (Input) có kích thước n. Trong đó, n có thể là số phần tử của mảng trong trường hợp bài toán sắp xếp hoặc tìm kiếm, hoặc có thể là độ lớn của số trong bài toán kiểm tra số nguyên tố, …

  -- Giả sử X là một giải thuật và n là kích cỡ của dữ liệu đầu vào. Thời gian và lượng bộ nhớ được sử dụng bởi giải thuật X là hai nhân tố chính quyết định hiệu quả của giải thuật X:

    +Nhân tố thời gian: Thời gian được đánh giá bằng việc tính số phép tính chính (chẳng hạn như các phép so sánh trong thuật toán sắp xếp).

    +Nhân tố bộ nhớ: Lượng bộ nhớ được đánh giá bằng việc tính lượng bộ nhớ tối đa mà giải thuật cần sử dụng.

  -- Độ phức tạp của một giải thuật (một hàm f(n)) cung cấp mối quan hệ giữa thời gian chạy và/hoặc lượng bộ nhớ cần được sử dụng bởi giải thuật.

# ✔️ Độ phức tạp bộ nhớ (Space complexity) trong phân tích giải thuật

  -- Nhân tố bộ nhớ của một giải thuật biểu diễn lượng bộ nhớ mà một giải thuật cần dùng trong vòng đời của giải thuật. Lượng bộ nhớ (giả sử gọi là S(P)) mà một giải thuật cần sử dụng là tổng của hai thành phần sau:

  + Phần cố định (giả sử gọi là C) là lượng bộ nhớ cần thiết để lưu giữ dữ liệu và các biến nào đó (phần này độc lập với kích cỡ của vấn đề). Ví dụ: các biến và các hằng đơn giản, …

  + Phần biến đổi (giả sử gọi là SP(I)) là lượng bộ nhớ cần thiết bởi các biến, có kích cỡ phụ thuộc vào kích cỡ của vấn đề. Ví dụ: cấp phát bộ nhớ động, cấp phát bộ nhớ đệ qui, …

# ✔️Độ phức tạp thời gian (Time Complexity) trong phân tích giải thuật

  -- Nhân tố thời gian của một giải thuật biểu diễn lượng thời gian chạy cần thiết từ khi bắt đầu cho đến khi kết thúc một giải thuật. Thời gian yêu cầu có thể được biểu diễn bởi một hàm T(n), trong đó T(n) có thể được đánh giá như là số các bước.

  -- Ví dụ, phép cộng hai số nguyên n-bit sẽ có n bước. Do đó, tổng thời gian tính toán sẽ là T(n) = c*n, trong đó c là thời gian để thực hiện phép cộng hai bit. Ở đây, chúng ta xem xét hàm T(n) tăng tuyến tính khi kích cỡ dữ liệu đầu vào tăng lên.

# -- Phân tích giải thuật tiệm cận trong cấu trúc dữ liệu và giải thuật 

-- Phân tích giải thuật tiệm cận là gì ? 

Phân tích tiệm cận của một giải thuật là k/n giúp chúng ta ước lượng thời gian chạy (running time) của một giải thuật. Sử dụng phân tích tiệm cận, chúng ta có thể đưa ra kết luận tôt nhất về các tình huống trường hợp tốt nhất, trường hợp trung bình, trường hợp xấu nhất của một giải thuật.

Phân tích tiệm cận tức là tiệm cận dữ liệu đầu vào input, tức là nếu giải thuật không có input thì kết luận cuối cùng sẽ là giải thuật chạy trong thời gian cụ thể và là hằng số. Ngoài nhân tố input các nhân tố khác được xem như là không đổi.

Phân tích tiệm cận nói đến việc ước lượng thời gian chạy của bất kỳ phép tính nào trong các bước tính toán. Ví dụ, thời gian chạy của một phép tính nào đó được ước lượng là một hàm f(n) và với một phép tính khác là hàm g(n2). Điều này có nghĩa là thời gian chạy của phép tính đầu tiên sẽ tăng tuyến tính với sự tăng lên của n và thời gian chạy của phép tính thứ hai sẽ tăng theo hàm mũ khi n tăng lên. Tương tự, khi n là khá nhỏ thì thời gian chạy của hai phép tính là gần như nhau.

Thường thì thời gian cần thiết bởi một giải thuật được chia thành 3 loại:

Trường hợp tốt nhất: là thời gian nhỏ nhất cần thiết để thực thi chương trình.

Trường hợp trung bình: là thời gian trung bình cần thiết để thực thi chương trình.

Trường hợp xấu nhất: là thời gian tối đa cần thiết để thực thi chương trình.
  
Dưới đây là các Asymptotic Notation được sử dụng phổ biến trong việc ước lượng độ phức tạp thời gian chạy của một giải thuật:

Ο Notation

Ω Notation

θ Notation

Big Oh Notation, Ο trong Cấu trúc dữ liệu và giải thuật

Ο(n) là một cách để biểu diễn tiệm cận trên của thời gian chạy của một thuật toán. Nó ước lượng độ phức tạp thời gian trường hợp xấu nhất hay chính là lượng thời gian dài nhất cần thiết bởi một giải thuật (thực thi từ bắt đầu cho đến khi kết thúc)

Omega Notation, Ω trong Cấu trúc dữ liệu và giải thuật
The Ω(n) là một cách để biểu diễn tiệm cận dưới của thời gian chạy của một giải thuật. Nó ước lượng độ phức tạp thời gian trường hợp tốt nhất hay chính là lượng thời gian ngắn nhất cần thiết bởi một giải thuật

Theta Notation, θ trong Cấu trúc dữ liệu và giải thuật
The θ(n) là cách để biểu diễn cả tiệm cận trên và tiệm cận dưới của thời gian chạy của một giải thuật

# Giải thuật tham lam (Greedy Algorithm) 

-- Giải thuật tham lam là gì ?
  + Tham lam (hay tham ăn) là một trong những phương pháp phổ biến nhất để thiết kế giải thuật. Nếu bạn đã đọc truyện dân gian thì sẽ có câu chuyện như thế này: trên một mâm cỗ có nhiều món ăn, món nào ngon nhất ta sẽ ăn trước, ăn hết món đó ta sẽ chuyển sang món ngon thứ hai, và chuyển tiếp sang món thứ ba, …

  + Rất nhiều giải thuật nổi tiếng được thiết kế dựa trên ý tưởng tham lam, ví dụ như giải thuật cây khung nhỏ nhất của Dijkstra, giải thuật cây khung nhỏ nhất của Kruskal, …

  + Giải thuật tham lam (Greedy Algorithm) là giải thuật tối ưu hóa tổ hợp. Giải thuật tìm kiếm, lựa chọn giải pháp tối ưu địa phương ở mỗi bước với hi vọng tìm được giải pháp tối ưu toàn cục.

  + Giải thuật tham lam lựa chọn giải pháp nào được cho là tốt nhất ở thời điểm hiện tại và sau đó giải bài toán con nảy sinh từ việc thực hiện lựa chọn đó. Lựa chọn của giải thuật tham lam có thể phụ thuộc vào lựa chọn trước đó. Việc quyết định sớm và thay đổi hướng đi của giải thuật cùng với việc không bao giờ xét lại các quyết định cũ sẽ dẫn đến kết quả là giải thuật này không tối ưu để tìm giải pháp toàn cục.

  -- Bạn theo dõi một bài toán đơn giản dưới đây để thấy cách thực hiện giải thuật tham lam và vì sao lại có thể nói rằng giải thuật này là không tối ưu.

  -- Bài toán đếm số đồng tiền
  + Yêu cầu là hãy lựa chọn số lượng đồng tiền nhỏ nhất có thể sao cho tổng mệnh giá của các đồng tiền này bằng với một lượng tiền cho trước.

  + Nếu tiền đồng có các mệnh giá lần lượt là 1, 2, 5, và 10 xu và lượng tiền cho trước là 18 xu thì giải thuật tham lam thực hiện như sau:

    +Bước 1: Chọn đồng 10 xu, do đó sẽ còn 18 – 10 = 8 xu.

    +Bước 2: Chọn đồng 5 xu, do đó sẽ còn là 3 xu.

    +Bước 3: Chọn đồng 2 xu, còn lại là 1 xu.

    +Bước 4: Cuối cùng chọn đồng 1 xu và giải xong bài toán.

  +Bạn thấy rằng cách làm trên là khá ổn, và số lượng đồng tiền cần phải lựa chọn là 4 đồng tiền. Nhưng nếu chúng ta thay đổi bài toán trên một chút thì cũng hướng tiếp cận như trên có thể sẽ không đem lại cùng kết quả tối ưu.

  +Chẳng hạn, một hệ thống tiền tệ khác có các đồng tiền có mệnh giá lần lượt là 1, 7 và 10 xu và lượng tiền cho trước ở đây thay đổi thành 15 xu thì theo giải thuật tham lam thì số đồng tiền cần chọn sẽ nhiều hơn 4. Với giải thuật tham lam thì: 10 + 1 + 1 +1 + 1 + 1, vậy tổng cộng là 6 đồng tiền. Trong khi cùng bài toán như trên có thể được xử lý bằng việc chỉ chọn 3 đồng tiền (7 + 7 +1).

  +Do đó chúng ta có thể kết luận rằng, giải thuật tham lam tìm kiếm giải pháp tôi ưu ở mỗi bước nhưng lại có thể thất bại trong việc tìm ra giải pháp tối ưu toàn cục.

  + Ví dụ áp dụng giải thuật tham lam
  Có khá nhiều giải thuật nổi tiếng được thiết kế dựa trên tư tưởng của giải thuật tham lam. Dưới đây là một trong số các giải thuật này:

  Bài toán hành trình người bán hàng
  Giải thuật cây khung nhỏ nhất của Prim
  Giải thuật cây khung nhỏ nhất của Kruskal
  Giải thuật cây khung nhỏ nhất của Dijkstra
  Bài toán xếp lịch công việc
  Bài toán xếp ba lô

# Giải thuật chia để trị (divide and conquer)

  -- Phương pháp chia để trị (Divide and Conquer) là một phương pháp quan trọng trong việc thiết kế các giải thuật. Ý tưởng của phương pháp này khá đơn giản và rất dễ hiểu: Khi cần giải quyết một bài toán, ta sẽ tiến hành chia bài toán đó thành các bài toán con nhỏ hơn. Tiếp tục chia cho đến khi các bài toán nhỏ này không thể chia thêm nữa, khi đó ta sẽ giải quyết các bài toán nhỏ nhất này và cuối cùng kết hợp giải pháp của tất cả các bài toán nhỏ để tìm ra giải pháp của bài toán ban đầu.

  + Nói chung, bạn có thể hiểu giải thuật chia để trị (Divide and Conquer) qua 3 tiến trình sau:

  + Tiến trình 1: Chia nhỏ (Divide/Break)
  Trong bước này, chúng ta chia bài toán ban đầu thành các bài toán con. Mỗi bài toán con nên là một phần của bài toán ban đầu. Nói chung, bước này sử dụng phương pháp đệ qui để chia nhỏ các bài toán cho đến khi không thể chia thêm nữa. Khi đó, các bài toán con được gọi là "atomic – nguyên tử", nhưng chúng vẫn biểu diễn một phần nào đó của bài toán ban đầu.

  + Tiến trình 2: Giải bài toán con (Conquer/Solve)
  Trong bước này, các bài toán con được giải.

  + Tiến trình 3: Kết hợp lời giải (Merge/Combine)
  Sau khi các bài toán con đã được giải, trong bước này chúng ta sẽ kết hợp chúng một cách đệ qui để tìm ra giải pháp cho bài toán ban đầu.

  Hạn chế của giải thuật chia để trị (Devide and Conquer)
  Giải thuật chia để trị tồn tại hai hạn chế, đó là:

  Làm thế nào để chia tách bài toán một cách hợp lý thành các bài toán con, bởi vì nếu các bài toán con được giải quyết bằng các thuật toán khác nhau thì sẽ rất phức tạp.

  Việc kết hợp lời giải các bài toán con được thực hiện như thế nào.

  ++ Ví dụ giải thuật chia để trị
  Dưới đây là một số giải thuật được xây dựng dựa trên phương pháp chia để trị (Divide and Conquer):

  Giải thuật sắp xếp trộn (Merge Sort)
  Giải thuật sắp xếp nhanh (Quick Sort)
  Giải thuật tìm kiếm nhị phân (Binary Search)
  Nhân ma trận của Strassen

  # Giải thuật Qui hoạch động (Dynamic Programming) là gì ?
  -- Giải thuật Qui hoạch động (Dynamic Programming) giống như giải thuật chia để trị (Divide and Conquer) trong việc chia nhỏ bài toán thành các bài toán con nhỏ hơn và sau đó thành các bài toán con nhỏ hơn nữa có thể. Nhưng không giống chia để trị, các bài toán con này không được giải một cách độc lập. Thay vào đó, kết quả của các bài toán con này được lưu lại và được sử dụng cho các bài toán con tương tự hoặc các bài toán con gối nhau (Overlapping Sub-problems).

  -- Chúng ta sử dụng Qui hoạch động (Dynamic Programming) khi chúng ta có các bài toán mà có thể được chia thành các bài toán con tương tự nhau, để mà các kết quả của chúng có thể được tái sử dụng. Thường thì các giải thuật này được sử dụng cho tối ưu hóa. Trước khi giải bài toán con, giải thuật Qui hoạch động sẽ cố gắng kiểm tra kết quả của các bài toán con đã được giải trước đó. Các lời giải của các bài toán con sẽ được kết hợp lại để thu được lời giải tối ưu.

  -- Do đó, chúng ta có thể nói rằng:

  Bài toán ban đầu nên có thể được phân chia thành các bài toán con gối nhau nhỏ hơn.

  Lời giải tối ưu của bài toán có thể thu được bởi sử dụng lời giải tối ưu của các bài toán con.

  Giải thuật Qui hoạch động sử dụng phương pháp lưu trữ (Memoization) – tức là chúng ta lưu trữ lời giải của các bài toán con đã giải, và nếu sau này chúng ta cần giải lại chính bài toán đó thì chúng ta có thể lấy và sử dụng kết quả đã được tính toán.

  -- So sánh
  + Giải thuật tham lam và giải thuật qui hoạch động
  Giải thuật tham lam (Greedy Algorithms) là giải thuật tìm kiếm, lựa chọn giải pháp tối ưu địa phương ở mỗi bước với hi vọng tìm được giải pháp tối ưu toàn cục.

  Giải thuật Qui hoạch động tối ưu hóa các bài toán con gối nhau.

  + Giải thuật chia để trị và giải thuật Qui hoạch động:
  Giải thuật chia để trị (Divide and Conquer) là kết hợp lời giải của các bài toán con để tìm ra lời giải của bài toán ban đầu.

  Giải thuật Qui hoạch động sử dụng kết quả của bài toán con và sau đó cố gắng tối ưu bài toán lớn hơn. Giải thuật Qui hoạch động sử dụng phương pháp lưu trữ (Memoization) để ghi nhớ kết quả của các bài toán con đã được giải.

  Ví dụ giải thuật Qui hoạch động
  Dưới đây là một số bài toán có thể được giải bởi sử dụng giải thuật Qui hoạch động:

  Dãy Fibonacci
  Bài toán tháp Hà Nội (Tower of Hanoi)
  Bài toán ba lô

  Giải thuật Qui hoạch động có thể được sử dụng trong cả hai phương pháp Phân tích (Top-down) và Qui nạp (Bottom-up). Và tất nhiên là nếu dựa vào vòng đời làm việc của CPU thì việc tham chiếu tới kết quả của lời giải trước đó là ít tốn kém hơn việc giải lại bài toán.

  # Giải thuật Định lý thợ (Master Theorem) là gì ?
  Chúng ta sử dụng Định lý thợ (Master Theorem) để giải các công thức đệ quy dạng sau một cách hiệu quả :

  T(n) =aT(n/b) + c.n^k trong đó a>=1 , b>1

  Bài toán ban đầu được chia thành a bài toán con có kích thước mỗi bài là n/b, chi phí để tổng hợp các bài toán con là f(n).

  Ví dụ : Thuật toán sắp xếp trộn chia thành 2 bài toán con , kích thước n/2. Chi phí tổng hợp 2 bài toán con là O(n).

  Định lý thợ
  a>=1, b>1, c, k là các hằng số. T(n) định nghĩa đệ quy trên các tham số không âm

  T(n) = aT(n/b) + c.n^k + Nếu a> b^k thì T(n) =O(n^ (logab)) + Nếu a= b^k thì T(n)=O(n^k.lgn) + Nếu a< b^k thì T(n) = O(n^k)

  Chú ý: Không phải trường hợp nào cũng áp dụng được định lý thợ

  VD : T(n) = 2T(n/2) +nlogn a =2, b =2, nhưng không xác định được số nguyên k

# -------------------------------------------------

# Cấu trúc dữ liệu mảng
-- Cấu trúc dữ liệu mảng là gì ?
+ Mảng (Array) là một trong các cấu trúc dữ liệu cũ và quan trọng nhất. Mảng có thể lưu giữ một số phần tử cố định và các phần tử này nền có cùng kiểu. Hầu hết các cấu trúc dữ liệu đều sử dụng mảng để triển khai giải thuật. Dưới đây là các khái niệm quan trọng liên quan tới Mảng.

+ Phần tử: Mỗi mục được lưu giữ trong một mảng được gọi là một phần tử.

+ Chỉ mục (Index): Mỗi vị trí của một phần tử trong một mảng có một chỉ mục số được sử dụng để nhận diện phần tử.

+ Mảng gồm các bản ghi có kiểu giống nhau, có kích thước cố định, mỗi phần tử được xác định bởi chỉ số

+ Mảng là cấu trúc dữ liệu được cấp phát lien tục cơ bản

-- Ưu điểm của mảng :
+ Truy câp phàn tử vơi thời gian hằng số O(1)

+ Sử dụng bộ nhớ hiệu quả

+ Tính cục bộ về bộ nhớ

-- Nhược điểm
+ Không thể thay đổi kích thước của mảng khi chương trình dang thực hiện

-- Mảng động
+ Mảng động (dynamic aray) : cấp phát bộ nhớ cho mảng một cách động trong quá trình chạy chương trình trong C là malloc và calloc, trong C++ là new

+ Sử dụng mảng động ta bắt đầu với mảng có 1 phàn tử, khi số lượng phàn tử vượt qua khả năng của ảng thì ta gấp đôi kích thước mảng cuc và copy phàn tử mảng cũ vào nửa đầu của mảng mới

-- Ưu điểm : tránh lãng phí bộ nhớ khi phải khai báo mảng có kích thước lớn ngay từ đầu

-- Nhược điểm: + phải thực hiện them thao tác copy phần tử mỗi khi thay đổi kích thước. + một số thời gian thực hiện thao tác không còn là hằng số nữa

Phép toán cơ bản được hỗ trợ bởi mảng
Dưới đây là các hoạt động cơ bản được hỗ trợ bởi một mảng:

Duyệt: In tất cả các phần tử mảng theo cách in từng phần tử một.

Chèn: Thêm một phần tử vào mảng tại chỉ mục đã cho.

Xóa: Xóa một phần tử từ mảng tại chỉ mục đã cho.

Tìm kiếm: Tìm kiếm một phần tử bởi sử dụng chỉ mục hay bởi giá trị.

Cập nhật: Cập nhật giá trị một phần tử tại chỉ mục nào đó.

-- Hoạt động chèn phần tử vào mảng
+ Hoạt động chèn là để chèn một hoặc nhiều phần tử dữ liệu vào trong một mảng. Tùy theo yêu cầu, phần tử mới có thể được chèn vào vị trí đầu, vị trí cuối hoặc bất kỳ vị trí chỉ mục đã cho nào của mảng.

+ Phần tiếp theo chúng ta sẽ cùng triển khai hoạt động chèn trong một ví dụ thực. Trong ví dụ này, chúng ta sẽ chèn dữ liệu vào cuối mảng.

-- Ví dụ

+ Giả sử LA là một mảng tuyến tính không có thứ tự có N phần tử và K là một số nguyên dương thỏa mãn K <= N. Dưới đây là giải thuật chèn phần tử A vào vị trí thứ K của mảng LA.

-- Giải thuật
```
- + 1. Bắt đầu
- + 2. Gán J=N
- + 3. Gán N = N+1
- + 4. Lặp lại bước 5 và 6 khi J >= K
- + 5. Gán LA[J+1] = LA[J]
- + 6. Gán J = J-1
- + 7. Gán LA[K] = ITEM
- + 8. Kết thúc
```

```
 #include <stdio.h>
 main() {
    int LA[] = {1,3,5,7,8};
    int item = 10, k = 3, n = 5;
    int i = 0, j = n;
    
    printf("Danh sach phan tu trong mang ban dau:\n");
 	
    for(i = 0; i<n; i++) {
       printf("LA[%d] = %d \n", i, LA[i]);
    }
     
    n = n + 1;
 	
    while( j >= k){
       LA[j+1] = LA[j];
       j = j - 1;
    }
 	
    LA[k] = item;
    
    printf("Danh sach phan tu cua mang sau hoat dong chen:\n");
 	
    for(i = 0; i<n; i++) {
       printf("LA[%d] = %d \n", i, LA[i]);
    }
 }
```

-- Hoạt động xóa phần tử từ mảng
+ Hoạt động xóa là xóa một phần tử đang tồn tại từ một mảng và tổ chức lại các phần tử còn lại trong mảng đó.

-- Ví dụ
+ Giả sử LA là một mảng tuyến tính có N phần tử và K là số nguyên dương thỏa mãn K <= N. Dưới đây là thuật toán để xóa một phần tử có trong mảng LA tại vị trí K.

-- Giải thuật
```
- + 1. Bắt đầu
- + 2. Gán J=K
- + 3. Lặp lại bước 4 và 5 trong khi J < N
- + 4. Gán LA[J-1] = LA[J]
- + 5. Gán J = J+1
- + 6. Gán N = N-1
- + 7. Kết thúc
```


-- Sau đây là code đầy đủ của giải thuật trên trong ngôn ngữ C:

```
#include <stdio.h>
 main() {
    int LA[] = {1,3,5,7,8};    
    
    int item = 5, n = 5;

   int i = 0, j = 0;
   
   printf("Danh sach phan tu trong mang ban dau:\n");
	
   for(i = 0; i<n; i++) {
       printf("LA[%d] = %d \n", i, LA[i]);
   }
     
   while( j < n){
 	
       if( LA[j] == item ){
          break;
       }
 		
       j = j + 1;
    }
 	
    printf("Tim thay phan tu %d tai vi tri %d\n", item, j+1);
 }
```



-- Hoạt động tìm kiếm
+ Bạn có thể thực hiện hoạt động tìm kiếm phần tử trong mảng dựa vào giá trị hay chỉ mục của phần tử đó.

-- Ví dụ
+ Giả sử LA là một mảng tuyến tính có N phần tử và K là số nguyên dương thỏa mãn K <= N. Dưới đây là giải thuật để tìm một phần tử ITEM bởi sử dụng phương pháp tìm kiếm tuần tự (hay tìm kiếm tuyến tính).

-- Giải thuật
```
- + 1. Bắt đầu
- + 2. Gán J=0
- + 3. Lặp lại bước 4 và 5 khi J < N
- + 4. Nếu LA[J] là bằng ITEM THÌ TỚI BƯỚC 6
- + 5. Gán J = J +1
- + 6. In giá trị J, ITEM
- + 7. Kết thúc
```



Sau đây là code đầy đủ của giải thuật trên trong ngôn ngữ C:

```
 #include <stdio.h>
    main() {
        int LA[] = {1,3,5,7,8};
        int item = 5, n = 5;
        int i = 0, j = 0;
        
        printf("Danh sach phan tu trong mang ban dau:\n");
      
        for(i = 0; i<n; i++) {
          printf("LA[%d] = %d \n", i, LA[i]);
        }
        
        while( j < n){
      
          if( LA[j] == item ){
              break;
          }
        
          j = j  1;
        }
      
        printf("Tim thay phan tu %d tai vi tri %d\n", item, j+1);
    }
```


-- Hoạt động cập nhật (Hoạt động update)
+ Hoạt động cập nhật là update giá trị của phần tử đang tồn tại trong mảng tại chỉ mục đã cho.

-- Giải thuật
+ Giả sử LA là một mảng tuyến tính có N phần tử và K là số nguyên dương thỏa mãn K <= N. Dưới đây là giải thuật để update giá trị phần tử tại vị trí K của mảng LA.

```
+ 1. Bắt đầu
+ 2. Thiết lập LA[K-1] = ITEM
+ 3. Kết thúc
```


-- Sau đây là code đầy đủ của giải thuật trên trong ngôn ngữ C:

```
#include <stdio.h>
 main() {
    int LA[] = {1,3,5,7,8};
    int k = 3, n = 5, item = 10;
    int i, j;
    
    printf("Danh sach phan tu trong mang ban dau:\n");
 	
    for(i = 0; i<n; i++) {
       printf("LA[%d] = %d \n", i, LA[i]);
    }
     
    LA[k-1] = item;
 
    printf("Danh sach phan tu trong mang sau hoat dong update:\n");
 	
    for(i = 0; i<n; i) {
       printf("LA[%d] = %d \n", i, LA[i]);
    }
 }
```

# Cấu trúc dữ liệu danh sách liên kết (Linked List)

-- Danh sách liên kết (Linked List) là gì ?

Một Danh sách liên kết (Linked List) là một dãy các cấu trúc dữ liệu được kết nối với nhau thông qua các liên kết (link). Hiểu một cách đơn giản thì Danh sách liên kết là một cấu trúc dữ liệu bao gồm một nhóm các nút (node) tạo thành một chuỗi. Mỗi nút gồm dữ liệu ở nút đó và tham chiếu đến nút kế tiếp trong chuỗi.

Danh sách liên kết là cấu trúc dữ liệu được sử dụng phổ biến thứ hai sau mảng. Dưới đây là các khái niệm cơ bản liên quan tới Danh sách liên kết:

```

+ Link (liên kết): mỗi link của một Danh sách liên kết có thể lưu giữ một dữ liệu được gọi là một phần tử.

+ Next: Mỗi liên kết của một Danh sách liên kết chứa một link tới next link được gọi là Next.

+ First: một Danh sách liên kết bao gồm các link kết nối tới first link được gọi là First.

```

-- Biểu diễn Danh sách liên kết (Linked List)



```

Dưới đây là một số điểm cần nhớ về Danh sách liên kết:

Danh sách liên kết chứa một phần tử link thì được gọi là First.
Mỗi link mang một trường dữ liệu và một trường link được gọi là Next.
Mỗi link được liên kết với link kế tiếp bởi sử dụng link kế tiếp của nó.
Link cuối cùng mang một link là null để đánh dấu điểm cuối của danh sách.

```

--Các loại Danh sách liên kết (Linked List)
Dưới đây là các loại Danh sách liên kết (Linked List) đa dạng:

```
Danh sách liên kết đơn (Simple Linked List): chỉ duyệt các phần tử theo chiều về trước.

Danh sách liên kết đôi (Doubly Linked List): các phần tử có thể được duyệt theo chiều về trước hoặc về sau.

Danh sách liên kết vòng (Circular Linked List): phần tử cuối cùng chứa link của phần tử đầu tiên như là next và phần tử đầu tiên có link tới phần tử cuối cùng như là prev.

```

---Các hoạt động cơ bản trên Danh sách liên kết
Dưới đây là một số hoạt động cơ bản có thể được thực hiện bởi một danh sách liên kết:

```
Hoạt động chèn: thêm một phần tử vào đầu danh sách liên kết.

Hoạt động xóa (phần tử đầu): xóa một phần tử tại đầu danh sách liên kết.

Hiển thị: hiển thị toàn bộ danh sách.

Hoạt động tìm kiếm: tìm kiếm phần tử bởi sử dụng khóa (key) đã cung cấp.

Hoạt động xóa (bởi sử dụng khóa): xóa một phần tử bởi sử dụng khóa (key) đã cung cấp.

```

# Cấu trúc dữ liệu và giải thuật Danh sách liên kết đôi

--Danh sách liên kết đôi (Doubly Linked List) là gì ?

+Danh sách liên kết đôi (Doubly Linked List) là một biến thể của Danh sách liên kết (Linked List), trong đó hoạt động duyệt qua các nút có thể được thực hiện theo hai chiều: về trước và về sau một cách dễ dàng khi so sánh với Danh sách liên kết đơn. Dưới đây là một số khái niệm quan trọng cần ghi nhớ về Danh sách liên kết đôi.

```
Link: mỗi link của một Danh sách liên kết có thể lưu giữ một dữ liệu và được gọi là một phần tử.

Next: mỗi link của một Danh sách liên kết có thể chứa một link tới next link và được gọi là Next.

Prev: mỗi link của một Danh sách liên kết có thể chứa một link tới previous link và được gọi là Prev.

First và Last: một Danh sách liên kết chứa link kết nối tới first link được gọi là First và tới last link được gọi là Last.

```
-- Biểu diễn Danh sách liên kết đôi

```

Danh sách liên kết đôi chứa một phần tử link và được gọi là First và Last.
Mỗi link mang một trường dữ liệu và một trường link được gọi là Next.
Mỗi link được liên kết với phần tử kế tiếp bởi sử dụng Next Link.
Mỗi link được liên kết với phần tử phía trước bởi sử dụng Prev Link.
Last Link mang một link trỏ tới NULL để đánh dầu phần cuối của Danh sách liên kết.

```

-- Các hoạt động cơ bản trên Danh sách liên kết đôi
Hoạt động chèn: thêm một phần tử vào vị trí đầu của Danh sách liên kết.

```
Hoạt động xóa: xóa một phần tử tại vị trí đầu của Danh sách liên kết.

Hoạt động chèn vào cuối: thêm một phần tử vào vị trí cuối của Danh sách liên kết.

Hoạt động xóa phần tử cuối: xóa một phần tử tại vị trí cuối của Danh sách liên kết.

Hoạt động chèn vào sau: thêm một phần tử vào sau một phần tử của Danh sách liên kết.

Hoạt động xóa (bởi key): xóa một phần tử từ Danh sách liên kết bởi sử dụng khóa đã cung cấp.

Hiển thị danh sách về phía trước: hiển thị toàn bộ Danh sách liên kết theo chiều về phía trước.

Hiển thị danh sách về phía sau: hiển thị toàn bộ Danh sách liên kết theo chiều về phía sau.

```

Hoạt động chèn trong Danh sách liên kết đôi

```
//Chèn link tại vị trí đầu tiên
void insertFirst(int key, int data) {

   //tạo một link
   struct node *link = (struct node*) malloc(sizeof(struct node));
   link->key = key;
   link->data = data;
	
   if(isEmpty()) {
      //Biến nó thành last link
      last = link;
   }else {
      //Cập nhật prev link đầu tiên
      head->prev = link;
   }

   //Trỏ nó tới first link cũ
   link->next = head;
	
   //Trỏ first tới first link mới
   head = link;
}
```

-- Hoạt động xóa trong Danh sách liên kết đôi

```
//xóa phần tử đầu tiên
struct node* deleteFirst() {

   //Lưu tham chiếu tới first link
   struct node *tempLink = head;
	
   //Nếu chỉ có link
   if(head->next == NULL) {
      last = NULL;
   }else {
      head->next->prev = NULL;
   }
	
   head = head->next;
	
   //Trả về link đã bị xóa
   return tempLink;
}

```

-- Hoạt động chèn tại vị trí cuối trong Danh sách liên kết đôi

```
//Chèn link vào vị trí cuối cùng
void insertLast(int key, int data) {

   //tạo một link
   struct node *link = (struct node*) malloc(sizeof(struct node));
   link->key = key;
   link->data = data;
	
   if(isEmpty()) {
      //biến nó thành last link
      last = link;
   }else {
      //làm cho link trở thành last link mới
      last->next = link;     
      //Đánh dấu last node là prev của new link
      link->prev = last;
   }

   //Trỏ last tới new last node
   last = link;
}
```

-- CHuong trinh day du

```
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

struct node {
   int data;
   int key;
	
   struct node *next;
   struct node *prev;
};

//link nay luon luon tro toi first Link 
struct node *head = NULL;

//link nay luon luon tro toi last Link 
struct node *last = NULL;

struct node *current = NULL;

//kiem tra xem list co trong khong
bool isEmpty(){
   return head == NULL;
}

int length(){
   int length = 0;
   struct node *current;
	
   for(current = head; current != NULL; current = current->next){
      length++;
   }
	
   return length;
}

//hien thi list bat dau tu first toi last
void displayForward(){

   //bat dau tu phan dau list
   struct node *ptr = head;
	
   //duyet toi cuoi list
   printf("\n[ ");
	
   while(ptr != NULL){        
      printf("(%d,%d) ",ptr->key,ptr->data);
      ptr = ptr->next;
   }
	
   printf(" ]");
}

//hien thi list bat dau tu last toi first
void displayBackward(){

   //bat dau tu cuoi list
   struct node *ptr = last;
	
   //duyet toi phan dau list
   printf("\n[ ");
	
   while(ptr != NULL){    
	
      //in du lieu
      printf("(%d,%d) ",ptr->key,ptr->data);
		
      //di chuyen toi phan tu tiep theo
      ptr = ptr ->prev;
      printf(" ");
   }
	
   printf(" ]");
}


//chen link tai vi tri dau tien
void insertFirst(int key, int data){

   //tao mot link
   struct node *link = (struct node*) malloc(sizeof(struct node));
   link->key = key;
   link->data = data;
	
   if(isEmpty()){
      //lam cho no thanh last link
      last = link;
   }else {
      //cap nhat first prev link
      head->prev = link;
   }

   //tro no toi first link cu
   link->next = head;
	
   //tro first toi first link moi
   head = link;
}

//chen link tai vi tri cuoi cung
void insertLast(int key, int data){

   //tao mot link
   struct node *link = (struct node*) malloc(sizeof(struct node));
   link->key = key;
   link->data = data;
	
   if(isEmpty()){
      //lam cho no thanh last link
      last = link;
   }else {
      //lam cho no thanh last link moi
      last->next = link;     
      //danh dau last node la prev cua link moi
      link->prev = last;
   }

   //tro last toi last node moi
   last = link;
}

//xoa phan tu dau tien
struct node* deleteFirst(){

   //luu tham chieu toi first link
   struct node *tempLink = head;
	
   //neu chi co link
   if(head->next == NULL){
      last = NULL;
   }else {
      head->next->prev = NULL;
   }
	
   head = head->next;
   //tra ve link da bi xoa
   return tempLink;
}

//xoa link tai vi tri cuoi cung

struct node* deleteLast(){
   //luu tham chieu toi last link
   struct node *tempLink = last;
	
   //neu chi co link
   if(head->next == NULL){
      head = NULL;
   }else {
      last->prev->next = NULL;
   }
	
   last = last->prev;
	
   //tra ve link bi xoa
   return tempLink;
}

//xoa mot link voi key da cho

struct node* deleteKey(int key){

   //bat dau tu link dau tien
   struct node* current = head;
   struct node* previous = NULL;
	
   //neu list la trong
   if(head == NULL){
      return NULL;
   }

   //duyet qua list
   while(current->key != key){
      //neu no la last node
		
      if(current->next == NULL){
         return NULL;
      }else {
         //luu tham chieu toi link hien tai
         previous = current;
			
         //di chuyen next link
         current = current->next;             
      }
   }

   //cap nhat link
   if(current == head) {
      //thay doi first de tro toi next link
      head = head->next;
   }else {
      //bo qua link hien tai
      current->prev->next = current->next;
   }    

   if(current == last){
      //thay doi last de tro toi prev link
      last = current->prev;
   }else {
      current->next->prev = current->prev;
   }
	
   return current;
}

bool insertAfter(int key, int newKey, int data){
   //bat dau tu first link
   struct node *current = head; 
	
   //neu list la trong
   if(head == NULL){
      return false;
   }

   //duyet qua list
   while(current->key != key){
	
      //neu day la last node
      if(current->next == NULL){
         return false;
      }else {           
         //di chuyen next link
         current = current->next;             
      }
   }
	
   //tao mot link
   struct node *newLink = (struct node*) malloc(sizeof(struct node));
   newLink->key = key;
   newLink->data = data;

   if(current == last) {
      newLink->next = NULL; 
      last = newLink; 
   }else {
      newLink->next = current->next;         
      current->next->prev = newLink;
   }
	
   newLink->prev = current; 
   current->next = newLink; 
   return true; 
}

main() {

   insertFirst(1,10);
   insertFirst(2,20);
   insertFirst(3,30);
   insertFirst(4,1);
   insertFirst(5,40);
   insertFirst(6,56); 

   printf("\nIn danh sach (First ---> Last): ");  
   displayForward();
	
   printf("\n");
   printf("\In danh sach (Last ---> first): "); 
   displayBackward();

   printf("\nDanh sach, sau khi xoa ban ghi dau tien: ");
   deleteFirst();        
   displayForward();

   printf("\nDanh sach, sau khi xoa ban ghi cuoi cung: ");  
   deleteLast();
   displayForward();

   printf("\nDanh sach, chen them phan tu sau key(4): ");  
   insertAfter(4,7, 13);
   displayForward();

   printf("\nDanh sach, sau khi xoa key(4) : ");  
   deleteKey(4);
   displayForward();
}
```
# Cấu trúc dữ liệu Danh sách liên kết vòng (Circular Linked List)

-- Danh sách liên kết vòng (Circular Linked List) là gì ?
```
Danh sách liên kết vòng (Circular Linked List) là một biến thể của Danh sách liên kết (Linked List), trong đó phần tử đầu tiên trỏ tới phần tử cuối cùng và phần tử cuối cùng trỏ tới phần tử đầu tiên.

Cả hai loại Danh sách liên kết đơn (Singly Linked List) và Danh sách liên kết đôi (Doubly Linked List) đều có thể được tạo thành dạng Danh sách liên kết vòng. Phần dưới chúng ta sẽ tìm hiểu từng cách tạo một.
```
-- Tạo Danh sách liên kết vòng từ Danh sách liên kết đơn
Trong Danh sách liên kết đơn, điểm trỏ tới kế tiếp của nút cuối sẽ trỏ tới nút đầu tiên, thay vì sẽ trỏ tới NULL.

-- Tạo Danh sách liên kết vòng từ Danh sách liên kết đôi
Trong Danh sách liên kết đôi, điểm trỏ tới kế tiếp của nút cuối trỏ tới nút đầu tiên và điểm trỏ tới phía trước của nút trước sẽ trỏ tới nút cuối cùng. Quá trình này sẽ tạo thành vòng ở cả hai hướng.
```
Next của Last Link trỏ tới First Link trong cả hai trường hợp với Danh sách liên kết đơn cũng như Danh sách liên kết đôi.

Prev của First Link trỏ tới phần tử cuối của Danh sách liên kết với trường hợp Danh sách liên kết đôi.
```

-- Các hoạt động cơ bản trên Danh sách liên kết vòng
Dưới đây là một số hoạt động cơ bản được hỗ trợ bởi Danh sách liên kết vòng:
```
Hoạt động chèn: chèn một phần tử vào vị trí bắt đầu của Danh sách liên kết vòng.

Hoạt động xóa: xóa một phần tử của Danh sách liên kết vòng.

Hiển thị: hiển thị toàn bộ Danh sách liên kết vòng.
```

 Hoạt động chèn trong Danh sách liên kết vòng
Dưới đây là giải thuật minh họa hoạt động chèn trong Danh sách liên kết vòng dựa trên Danh sách liên kết đơn.
```
//Chèn link tại vị trí đầu tiên
void insertFirst(int key, int data) {
   //tạo một link
   struct node *link = (struct node*) malloc(sizeof(struct node));
   link->key = key;
   link->data= data;
	
   if (isEmpty()) {
      head = link;
      head->next = head;
   }else {
      //trỏ nó tới first node cũ
      link->next = head;
		
      //trỏ first tới first node mới
      head = link;
   }   
   
}

```

-- Hoạt động xóa trong Danh sách liên kết vòng
Dưới đây là giải thuật minh họa hoạt động xóa trong Danh sách liên kết vòng dựa trên Danh sách liên kết đơn.
```
//Xóa phần tử đầu tiên
struct node * deleteFirst() {
   //Lưu tham chiếu tới first link
   struct node *tempLink = head;
	
   if(head->next == head){  
      head = NULL;
      return tempLink;
   }     

   //Đánh dấu next tới first link là first 
   head = head->next;
	
   //trả về link đã bị xóa
   return tempLink;
}
```

-- Hiển thị Danh sách liên kết vòng
Dưới đây là giải thuật minh họa hoạt động hiển thị toàn bộ Danh sách liên kết vòng.

```
//Hiển thị danh sách liên kết vòng
void printList() {
   struct node *ptr = head;
   printf("\n[ ");
	
   //Bắt đầu từ vị trí đầu tiên
   if(head != NULL) {
      while(ptr->next != ptr) {     
         printf("(%d,%d) ",ptr->key,ptr->data);
         ptr = ptr->next;
      }
   }
	
   printf(" ]");
}

```

chuong trinh day du

```
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

struct node {
   int data;
   int key;
	
   struct node *next;
};

struct node *head = NULL;
struct node *current = NULL;

bool isEmpty(){
   return head == NULL;
}

int length(){
   int length = 0;

   //neu danh sach la trong
   if(head == NULL){
      return 0;
   }

   current = head->next;

   while(current != head){
      length++;
      current = current->next;   
   }
	
   return length;
}

//chen link tai vi tri dau tien
void insertFirst(int key, int data){

   //tao mot link
   struct node *link = (struct node*) malloc(sizeof(struct node));
   link->key = key;
   link->data = data;
	
   if (isEmpty()) {
      head = link;
      head->next = head;
   }else {
      //tro no toi first node cu
      link->next = head;
		
      //tro first toi first node moi
      head = link;
   }    
	
}

//xoa phan tu dau tien
struct node * deleteFirst(){

   //luu tham chieu toi first link
   struct node *tempLink = head;
	
   if(head->next == head){  
      head = NULL;
      return tempLink;
   }     

   //danh dau next toi first link la first 
   head = head->next;
	
   //tra ve link da bi xoa
   return tempLink;
}

//hien thi danh sach
void printList(){

   struct node *ptr = head;
   printf("\n[ ");
	
   //bat dau tu phan dau cua danh sach
   if(head != NULL){
	
      while(ptr->next != ptr){     
         printf("(%d,%d) ",ptr->key,ptr->data);
         ptr = ptr->next;
      }
		
   }
	
   printf(" ]");
}

main() {

   insertFirst(1,10);
   insertFirst(2,20);
   insertFirst(3,30);
   insertFirst(4,1);
   insertFirst(5,40);
   insertFirst(6,56); 

   printf("Danh sach ban dau: "); 
	
   //In danh sach
   printList();

   while(!isEmpty()){            
      struct node *temp = deleteFirst();
      printf("\nGia tri bi xoa:");  
      printf("(%d,%d) ",temp->key,temp->data);        
   }   
	
   printf("\nDanh sach sau khi da xoa tat ca phan tu: ");          
   printList();   
}
```

# ------------------------------------------------

# Cấu trúc dữ liệu ngăn xếp (Stack)

-- Ngăn xếp (Stack) là gì ?

Một ngăn xếp là một cấu trúc dữ liệu trừu tượng (Abstract Data Type – viết tắt là ADT), hầu như được sử dụng trong hầu hết mọi ngôn ngữ lập trình. Đặt tên là ngăn xếp bởi vì nó hoạt động như một ngăn xếp trong đời sống thực, ví dụ như một cỗ bài hay một chồng đĩa, …

Trong đời sống thực, ngăn xếp chỉ cho phép các hoạt động tại vị trí trên cùng của ngăn xếp. Ví dụ, chúng ta chỉ có thể đặt hoặc thêm một lá bài hay một cái đĩa vào trên cùng của ngăn xếp. Do đó, cấu trúc dữ liệu trừu tượng ngăn xếp chỉ cho phép các thao tác dữ liệu tại vị trí trên cùng. Tại bất cứ thời điểm nào, chúng ta chỉ có thể truy cập phần tử trên cùng của ngăn xếp.

Đặc điểm này làm cho ngăn xếp trở thành cấu trúc dữ liệu dạng LIFO. LIFO là viết tắt của Last-In-First-Out. Ở đây, phần tử được đặt vào (được chèn, được thêm vào) cuối cùng sẽ được truy cập đầu tiên. Trong thuật ngữ ngăn xếp, hoạt động chèn được gọi là hoạt động PUSH và hoạt động xóa được gọi là hoạt động POP.

-- Biểu diễn cấu trúc dữ liệu ngăn xếp (Stack)

Một ngăn xếp có thể được triển khai theo phương thức của Mảng (Array), Cấu trúc (Struct), Con trỏ (Pointer) và Danh sách liên kết (Linked List). Ngăn xếp có thể là ở dạng kích cỡ cố định hoặc ngăn xếp có thể thay đổi kích cỡ. Phần dưới chúng ta sẽ triển khai ngăn xếp bởi sử dụng các mảng với việc triển khai các ngăn xếp cố định.

-- Các hoạt động cơ bản trên cấu trúc dữ liệu ngăn xếp
Các hoạt động cơ bản trên ngăn xếp có thể liên quan tới việc khởi tạo ngăn xếp, sử dụng nó và sau đó xóa nó. Ngoài các hoạt động cơ bản này, một ngăn xếp có hai hoạt động nguyên sơ liên quan tới khái niệm, đó là:

```
Hoạt động push(): lưu giữ một phần tử trên ngăn xếp.

Hoạt động pop(): xóa một phần tử từ ngăn xếp.
```

Khi dữ liệu đã được PUSH lên trên ngăn xếp:

Để sử dụng ngăn xếp một cách hiệu quả, chúng ta cũng cần kiểm tra trạng thái của ngăn xếp. Để phục vụ cho mục đích này, dưới đây là một số tính năng hỗ trợ khác của ngăn xếp:

```
Hoạt động peek(): lấy phần tử dữ liệu ở trên cùng của ngăn xếp, mà không xóa phần tử này.

Hoạt động isFull(): kiểm tra xem ngăn xếp đã đầy hay chưa.

Hoạt động isEmpty(): kiểm tra xem ngăn xếp là trống hay không.
```

Tại mọi thời điểm, chúng ta duy trì một con trỏ tới phần tử dữ liệu vừa được PUSH cuối cùng vào trên ngăn xếp. Vì con trỏ này luôn biểu diễn vị trí trên cùng của ngăn xếp vì thế được đặt tên là top. Con trỏ top cung cấp cho chúng ta giá trị của phần tử trên cùng của ngăn xếp mà không cần phải thực hiện hoạt động xóa ở trên (hoạt động pop).

Phần tiếp theo chúng ta sẽ tìm hiểu về các phương thức để hỗ trợ các tính năng của ngăn xếp.

Phương thức peek() của cấu trúc dữ liệu ngăn xếp
Giải thuật của hàm peek():

```
Bắt đầu hàm peek

   return stack[top]
   
kết thúc hàm
```
Sự triển khai của hàm peek() trong ngôn ngữ C:
```
int peek() {
   return stack[top];
}
```



Phương thức isFull() của cấu trúc dữ liệu ngăn xếp
Giải thuật của hàm isFull():

```
Bắt đầu hàm isfull

   if top bằng MAXSIZE
      return true
   else
      return false
   kết thúc if
   
kết thúc hàm
```
Sự triển khai của hàm isFull() trong ngôn ngữ C:
```
bool isfull() {
   if(top == MAXSIZE)
      return true;
   else
      return false;
}
```

Phương thức isEmpty() của cấu trúc dữ liệu ngăn xếp
Giải thuật của hàm isEmpty():
```
bắt đầu hàm isempty

   if top nhỏ hơn 1
      return true
   else
      return false
   kết thúc if
   
kết thúc hàm
```
Sự triển khai của hàm isEmpty() trong ngôn ngữ C khác hơn một chút. Chúng ta khởi tạo top tại -1, giống như chỉ mục của mảng bắt đầu từ 0. Vì thế chúng ta kiểm tra nếu top là dưới 0 hoặc -1 thì ngăn xếp là trống. Dưới đây là phần code:
```
bool isempty() {
   if(top == -1)
      return true;
   else
      return false;
}
```

--Hoạt động PUSH trong cấu trúc dữ liệu ngăn xếp
Tiến trình đặt (thêm) một phần tử dữ liệu mới vào trên ngăn xếp còn được biết đến với tên Hoạt động PUSH. Hoạt động push bao gồm các bước sau:

```
Bước 1: kiểm tra xem ngăn xếp đã đầy hay chưa.

Bước 2: nếu ngăn xếp là đầy, tiến trình bị lỗi và thoát ra.

Bước 3: nếu ngăn xếp chưa đầy, tăng top để trỏ tới phần bộ nhớ trống tiếp theo.

Bước 4: thêm phần tử dữ liệu vào vị trí nơi mà top đang trỏ đến trên ngăn xếp.

Bước 5: trả về success.
```


Giải thuật cho hoạt động PUSH của cấu trúc dữ liệu ngăn xếp
Từ trên có thể suy ra một giải thuật đơn giản cho hoạt động PUSH trong cấu trúc dữ liệu ngăn xếp như sau:

```
bắt đầu hoạt động push: stack, data

   if stack là đầy
      return null
   kết thúc if
   
   top ← top + 1
   
   stack[top] ← data

kết thúc hàm
```
Sự triển khai của giải thuật này trong ngôn ngữ C là:
```
void push(int data) {
   if(!isFull()) {
      top = top + 1;   
      stack[top] = data;
   }else {
      printf("Khong the chen them du lieu vi Stack da day.\n");
   }
}

```

-- Chương trình đầy đủ

```
#include <stdio.h>

int MAXSIZE = 8;       
int stack[8];     
int top = -1;            

int isempty() {

   if(top == -1)
      return 1;
   else
      return 0;
}
   
int isfull() {

   if(top == MAXSIZE)
      return 1;
   else
      return 0;
}

int peek() {
   return stack[top];
}


int pop() {
   int data;
	
   if(!isempty()) {
      data = stack[top];
      top = top - 1;   
      return data;
   }else {
      printf("Khong the thu thap du lieu, ngan xep (Stack) la trong.\n");
   }
}

int push(int data) {

   if(!isfull()) {
      top = top + 1;   
      stack[top] = data;
   }else {
      printf("Khong the chen du lieu, ngan xep (Stack) da day.\n");
   }
}

int main() {
   // chen cac phan tu vao ngan xep
   push(3);
   push(5);
   push(9);
   push(1);
   push(12);
   push(15);

   printf("Phan tu tai vi tri tren cung cua ngan xep: %d\n" ,peek());
   printf("Cac phan tu: \n");

   // in cac phan tu trong ngan xep
   while(!isempty()) {
      int data = pop();
      printf("%d\n",data);
   }

   printf("Ngan xep da day: %s\n" , isfull()?"true":"false");
   printf("Ngan xep la trong: %s\n" , isempty()?"true":"false");
   
   return 0;
}
```
-- Hoạt động POP của cấu trúc dữ liệu ngăn xếp
Việc truy cập nội dung phần tử trong khi xóa nó từ ngăn xếp còn được gọi là Hoạt động POP. Trong sự triển khai Mảng của hoạt động pop(), phần tử dữ liệu không thực sự bị xóa, thay vào đó top sẽ bị giảm về vị trí thấp hơn trong ngăn xếp để trỏ tới giá trị tiếp theo. Nhưng trong sự triển khai Danh sách liên kết, hoạt động pop() thực sụ xóa phần tử xữ liệu và xóa nó khỏi không gian bộ nhớ.

Hoạt động POP có thể bao gồm các bước sau:
```
Bước 1: kiểm tra xem ngăn xếp là trống hay không.

Bước 2: nếu ngăn xếp là trống, tiến trình bị lỗi và thoát ra.

Bước 3: nếu ngăn xếp là không trống, truy cập phần tử dữ liệu tại top đang trỏ tới.

Bước 4: giảm giá trị của top đi 1.

Bước 5: trả về success.

```

Giải thuật cho hoạt động POP
Từ trên ta có thể suy ra giải thuật cho hoạt động POP trên cấu trúc dữ liệu ngăn xếp như sau:
```
bắt đầu hàm pop: stack

   if stack là trống
      return null
   kết thúc if
   
   data ← stack[top]
   
   top ← top - 1
   
   return data

kết thúc hàm

```
Sự triển khai giải thuật trong ngôn ngữ C như sau:
```
int pop(int data) {

   if(!isempty()) {
      data = stack[top];
      top = top - 1;   
      return data;
   }else {
      printf("Khong the lay du lieu, Stack la trong.\n");
   }
}
```

-- Chương trình đầy đủ

```
#include <stdio.h>

int MAXSIZE = 8;       
int stack[8];     
int top = -1;            

int isempty() {

   if(top == -1)
      return 1;
   else
      return 0;
}
   
int isfull() {

   if(top == MAXSIZE)
      return 1;
   else
      return 0;
}

int peek() {
   return stack[top];
}


int pop() {
   int data;
	
   if(!isempty()) {
      data = stack[top];
      top = top - 1;   
      return data;
   }else {
      printf("Khong the thu thap du lieu, ngan xep (Stack) la trong.\n");
   }
}

int push(int data) {

   if(!isfull()) {
      top = top + 1;   
      stack[top] = data;
   }else {
      printf("Khong the chen du lieu, ngan xep (Stack) da day.\n");
   }
}

int main() {
   // chen cac phan tu vao ngan xep
   push(3);
   push(5);
   push(9);
   push(1);
   push(12);
   push(15);

   printf("Phan tu tai vi tri tren cung cua ngan xep: %d\n" ,peek());
   printf("Cac phan tu: \n");

   // in cac phan tu trong ngan xep
   while(!isempty()) {
      int data = pop();
      printf("%d\n",data);
   }

   printf("Ngan xep da day: %s\n" , isfull()?"true":"false");
   printf("Ngan xep la trong: %s\n" , isempty()?"true":"false");
   
   return 0;
}
```

Ứng dụng của ngăn xếp
- Xử lý gọi hàm trong C/C++ - Trong máy tính, sử dụng để tính giá trị biểu thức, xử lý ngắt - Trong các chương trình biên dịch - Trong trình duyệt web, trình soạn thảo văn bản - Định giá biểu thức + Biểu thức trung tố: toán tử hai ngôi đứng giưã hai toán hạng, toán tử một ngôi đứng trước toán hạng + Biểu thức hậu tố : toán tử đứng sau toán hạng + Biểu thức tiền tố : toán tử đứng trước toán hạng

VD định giá biểu thức A = b + c * d /e – f Trung tố a*(b-c)/d Hậu tố abc-*d/ Tiền tố /*a-bcd

Duyệt biểu thức hậu tố :
- Gặp toán hạng : đẩy vào stack - Gặp toán tử 1 ngôi : lấy ra 1 toán hạng trong stack, áp dụng toán tử lên toán hạng và đấy kết quả trở lại stack - Gặp toán tử 2 ngôi :lấy 2 toán hạng ở đỉnh stack theo thứ tự, áp dụng toán tử lên 2 toán hạng đó, kết quả lại đẩy vào stack - Kết thúc, đưa ra kết quả là giá trị ở đỉnh stack - Vd định giá biểu thức hậu tố

Duyệt biểu thức hậu tố trong cấu trúc dữ liệu ngăn xếp
Chuyển biểu thức dạng trung tố sang hậu tố
- Duyệt lần lượt biểu thưc trung tố từ trái qua phải - Gặp toán hạng : viết sang biểu thức kết quả - Gặp toán tử có độ ưu tiên nhỏ hơn 6 + Nếu stack rỗng hoặc đỉnh stack là toán tử có độ ưu tiên nhỏ hơn hoặc là '(' đẩy toán tử đang xét vào stack + Ngược lại : lấy các toán tử ở đỉnh stack có độ ưu tiên lớn hơn hoặc bằng toán tử đang xét lần lượt đưa vào nbieeur thức kết quả và đẩy toán tử đang xét vào stack - Gặp toán tử có đooj ưu tiên 6 hoăc '(' thì đẩy vào stack - Gặp ')' lấy tất cả các toán tử trong stack cho đến khi gặp '(' đầu tiên, đưa sang biểu thức kết quả theo đúng thứ tự và đẩy 1 kí hiệu '(' ra khỏi stack - Nếu duyệt hết biểu thức trung tố, lấy nốt những toán tử trong stack đưa sang biểu thức kết quả theo đúng thứ tự.

# Cấu trúc dữ liệu hàng đợi (Queue)

--Cấu trúc dữ liệu hàng đợi (Queue) là gì ?

Hàng đợi (Queue) là một cấu trúc dữ liệu trừu tượng, là một cái gì đó tương tự như hàng đợi trong đời sống hàng ngày (xếp hàng).

Khác với ngăn xếp, hàng đợi là mở ở cả hai đầu. Một đầu luôn luôn được sử dụng để chèn dữ liệu vào (hay còn gọi là sắp vào hàng) và đầu kia được sử dụng để xóa dữ liệu (rời hàng). Cấu trúc dữ liệu hàng đợi tuân theo phương pháp First-In-First-Out, tức là dữ liệu được nhập vào đầu tiên sẽ được truy cập đầu tiên.

Trong đời sống thực chúng ta có rất nhiều ví dụ về hàng đợi, chẳng hạn như hàng xe ô tô trên đường một chiều (đặc biệt là khi tắc xe), trong đó xe nào vào đầu tiên sẽ thoát ra đầu tiên. Một vài ví dụ khác là xếp hàng học sinh, xếp hàng mua vé, …

--Biểu diễn cấu trúc dữ liệu hàng đợi (Queue)

Giờ thì có lẽ bạn đã tưởng tượng ra hàng đợi là gì rồi. Chúng ta có thể truy cập cả hai đầu của hàng đợi. Dưới đây là biểu diễn hàng đợi dưới dạng cấu trúc dữ liệu:


Tương tự như cấu trúc dữ liệu ngăn xếp, thì cấu trúc dữ liệu hàng đợi cũng có thể được triển khai bởi sử dụng Mảng (Array), Danh sách liên kết (Linked List), Con trỏ (Pointer) và Cấu trúc (Struct). Để đơn giản, phần tiếp theo chúng ta sẽ tìm hiểu tiếp về hàng đợi được triển khai bởi sử dụng mảng một chiều.

-- Các hoạt động cơ bản trên cấu trúc dữ liệu hàng đợi

+ Các hoạt động trên cấu trúc dữ liệu hàng đợi có thể liên quan tới việc khởi tạo hàng đợi, sử dụng dữ liệu trên hàng đợi và sau đó là xóa dữ liệu khỏi bộ nhớ. Danh sách dưới đây là một số hoạt động cơ bản có thể thực hiện trên cấu trúc dữ liệu hàng đợi:
```
Hoạt động enqueue(): thêm (hay lưu trữ) một phần tử vào trong hàng đợi.

Hoạt động dequeue(): xóa một phần tử từ hàng đợi.
```

+ Để sử dụng hàng đợi một cách hiệu quả, chúng ta cũng cần kiểm tra trạng thái của hàng đợi. Để phục vụ cho mục đích này, dưới đây là một số tính năng hỗ trợ khác của hàng đợi:
```
Phương thức peek(): lấy phần tử ở đầu hàng đợi, mà không xóa phần tử này.

Phương thức isFull(): kiểm tra xem hàng đợi là đầy hay không.

Phương thức isEmpty(): kiểm tra xem hàng đợi là trống hay hay không.
```

Trong cấu trúc dữ liệu hàng đợi, chúng ta luôn luôn: (1) dequeue (xóa) dữ liệu được trỏ bởi con trỏ front và (2) enqueue (nhập) dữ liệu vào trong hàng đợi bởi sự giúp đỡ của con trỏ rear.

Trong phần tiếp chúng ta sẽ tìm hiểu về các tính năng hỗ trợ của cấu trúc dữ liệu hàng đợi:

Phương thức peek() của cấu trúc dữ liệu hàng đợi
Giống như trong cấu trúc dữ liệu ngăn xếp, hàm này giúp chúng ta quan sát dữ liệu tại đầu hàng đợi. Giải thuật của hàm peek() là:
```
bắt đầu hàm peek

   return queue[front]
   
kết thúc hàm
```
Sự triển khai của hàm peek() trong ngôn ngữ C:
```
int peek() {
   return queue[front];
}
```

Phương thức isFull() trong cấu trúc dữ liệu hàng đợi
Nếu khi chúng ta đang sử dụng mảng một chiều để triển khai hàng đợi, chúng ta chỉ cần kiểm tra con trỏ rear có tiến đến giá trị MAXSIZE hay không để xác định hàng đợi là đầy hay không. Trong trường hợp triển khai hàng đợi bởi sử dụng Danh sách liên kết vòng (Circular Linked List), giải thuật cho hàm isFull() sẽ khác.

Phần dưới đây là giải thuật của hàm isFull():
```
bắt đầu hàm isfull

   if rear equals to MAXSIZE
      return true
   else
      return false
   endif
   
kết thúc hàm
```
Sự triển khai giải thuật của hàm isFull() trong ngôn ngữ C:
```
bool isfull() {
   if(rear == MAXSIZE - 1)
      return true;
   else
      return false;
}
```
Phương thức isEmpty() trong cấu trúc dữ liệu hàng đợi
Giải thuật của hàm isEmpty():
```
bắt đầu hàm isempty

   if front là nhỏ hơn MIN  OR front là lớn hơn rear
      return true
   else
      return false
   kết thúc if
   
kết thúc hàm
```
Nếu giá trị của front là nhỏ hơn MIN hoặc 0 thì tức là hàng đợi vẫn chưa được khởi tạo, vì thế hàng đợi là trống.

Dưới đây là sự triển khai code trong ngôn ngữ C:
```
bool isempty() {
   if(front < 0 || front > rear) 
      return true;
   else
      return false;
}
```

-- Hoạt động enqueue trong cấu trúc dữ liệu hàng đợi
Bởi vì cấu trúc dữ liệu hàng đợi duy trì hai con trỏ dữ liệu: front và rear, do đó các hoạt động của loại cấu trúc dữ liệu này là khá phức tạp khi so sánh với cấu trúc dữ liệu ngăn xếp.

Dưới đây là các bước để enqueue (chèn) dữ liệu vào trong hàng đợi:
```
Bước 1: kiểm tra xem hàng đợi là có đầy không.

Bước 2: nếu hàng đợi là đầy, tiến trình bị lỗi và bị thoát.

Bước 3: nếu hàng đợi không đầy, tăng con trỏ rear để trỏ tới vị trí bộ nhớ trống tiếp theo.

Bước 4: thêm phần tử dữ liệu vào vị trí con trỏ rear đang trỏ tới trong hàng đợi.

Bước 5: trả về success.
```

Giải thuật cho hoạt động enqueue trong cấu trúc dữ liệu hàng đợi
```
bắt đầu enqueue(data)      
   if queue là đầy
      return overflow
   endif
   
   rear ← rear + 1
   
   queue[rear] ← data
   
   return true
   
kết thúc hàm
```
Sự triển khai giải thuật của hoạt động enqueue() trong ngôn ngữ C:
```
int enqueue(int data)      
   if(isfull())
      return 0;
   
   rear = rear + 1;
   queue[rear] = data;
   
   return 1;
kết thúc hàm
```

Hoạt động dequeue trong cấu trúc dữ liệu hàng đợi
Việc truy cập dữ liệu từ hàng đợi là một tiến trình gồm hai tác vụ: truy cập dữ liệu tại nơi con trỏ front đang trỏ tới và xóa dữ liệu sau khi đã truy cập đó. Dưới đây là các bước để thực hiện hoạt động dequeue:
```
Bước 1: kiểm tra xem hàng đợi là trống hay không.

Bước 2: nếu hàng đợi là trống, tiến trình bị lỗi và bị thoát.

Bước 3: nếu hàng đợi không trống, truy cập dữ liệu tại nơi con trỏ front đang trỏ.

Bước 4: tăng con trỏ front để trỏ tới vị trí chứa phần tử tiếp theo.

Bước 5: trả về success.
```

Giải thuật cho hoạt động dequeue
```
bắt đầu hàm dequeue
   if queue là trống
      return underflow
   end if

   data = queue[front]
   front ← front + 1
   
   return true
kết thúc hàm
```
Sự triển khai hoạt động dequeue() trong ngôn ngữ C:
```
int dequeue() {

   if(isempty())
      return 0;

   int data = queue[front];
   front = front + 1;

   return data;
}

```

vd C: 

```
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX 6

int intArray[MAX];
int front = 0;
int rear = -1;
int itemCount = 0;

int peek(){
   return intArray[front];
}

bool isEmpty(){
   return itemCount == 0;
}

bool isFull(){
   return itemCount == MAX;
}

int size(){
   return itemCount;
}  

void insert(int data){

   if(!isFull()){
	
      if(rear == MAX-1){
         rear = -1;            
      }       

      intArray[++rear] = data;
      itemCount++;
   }
}

int removeData(){
   int data = intArray[front++];
	
   if(front == MAX){
      front = 0;
   }
	
   itemCount--;
   return data;  
}

int main() {
   /* chen 5 phan tu */
   insert(3);
   insert(5);
   insert(9);
   insert(1);
   insert(12);

   // front : 0
   // rear  : 4
   // ------------------
   // index : 0 1 2 3 4 
   // ------------------
   // queue : 3 5 9 1 12
   insert(15);

   // front : 0
   // rear  : 5
   // ---------------------
   // index : 0 1 2 3 4  5 
   // ---------------------
   // queue : 3 5 9 1 12 15
	
   if(isFull()){
      printf("Hang doi (Queue) da day!\n");   
   }

   // xoa mot phan tu 
   int num = removeData();
	
   printf("Phan tu bi xoa: %d\n",num);
   // front : 1
   // rear  : 5
   // -------------------
   // index : 1 2 3 4  5
   // -------------------
   // queue : 5 9 1 12 15

   // Chen them mot phan tu
   insert(16);

   // front : 1
   // rear  : -1
   // ----------------------
   // index : 0  1 2 3 4  5
   // ----------------------
   // queue : 16 5 9 1 12 15

   // neu hang doi la day thi phan tu se khong duoc chen. 
   insert(17);
   insert(18);

   // ----------------------
   // index : 0  1 2 3 4  5
   // ----------------------
   // queue : 16 5 9 1 12 15
   printf("Phan tu tai vi tri front: %d\n",peek());

   printf("----------------------\n");
   printf("Gia tri chi muc : 5 4 3 2  1  0\n");
   printf("----------------------\n");
   printf("Hang doi (Queue):  ");
	
   while(!isEmpty()){
      int n = removeData();           
      printf("%d ",n);
   }   
}
```


# Vấn đề lưu nhiều ảnh sản phẩm làm thế nào ????
- Shoppee lưu id ảnh

images: ["jahsdjashkdjashdkjash","adhjakshdkjahsdkjahsd"]

- làm cách nào để liên kết lại với nhau

# Flex Box: Property

display: flex | inline-flex;
flex-direction: row | column;
flex-wrap: nowrap | wrap | wrap-reverse;
flex-basis: <length>;
justify-content: flex-start| flex-end | center | space-between| space-around | ...; //main axis using give class parent 
justify-self: flex-start| flex-end | center | space-between| space-around | ...; //main axis using give class children
align-content: flex-start | flex-end | center // cross-axis parent
align-self: flex-start | flex-end | center // children
flex-grow: <number> // độ nở
flex-shrink: <number> // độ thu
flex: <number> // shorthand flex-grow flex-shrink flex-basis
order: <number>; // quyết định thứ tự hiển thị
flex-flow: // shorthand flex-direction flex-wrap

Grid System

diplay: grid | inline-grid; // full-col | 1 - col
grid-column-gap: <number>;
grid-row-gap: <number>;
grid-gap: <number> <number>; // shorthand grid-column-gap | grid-row-gap
grid-column-start: <number>;
grid-column-end: <number> <=> span <number - 1>;
grid-column: <number> / <number> <=> <number> <=> span <number - 1>; // shorthand;
```
      Property	                                       Description
-- column-gap	            Specifies the gap between the columns
-- row-gap	               Specifies the gap between the grid rows
-- gap	                  A shorthand property for the row-gap and the column-gap properties
-- grid	                  A shorthand property for the grid-template-rows, grid-template-columns, grid-template-areas, grid-auto-rows,      grid-auto-columns, and the grid-auto-flow properties //   grid: 150px / auto auto auto;

-- grid-area	            Either specifies a name for the grid item, or this property is a shorthand property for the grid-row-start, grid-column-start, grid-row-end, and grid-column-end properties

-- grid-auto-columns	      Specifies a default column size
-- grid-auto-flow	         Specifies how auto-placed items are inserted in the grid
-- grid-auto-rows	         Specifies a default row size
-- grid-column	            A shorthand property for the grid-column-start and the grid-column-end properties
-- grid-column-end	      Specifies where to end the grid item
-- grid-column-gap	      Specifies the size of the gap between columns
-- grid-column-start	      Specifies where to start the grid item
-- grid-gap	               A shorthand property for the grid-row-gap and grid-column-gap properties
-- grid-row	               A shorthand property for the grid-row-start and the grid-row-end properties
-- grid-row-end	         Specifies where to end the grid item
-- grid-row-gap	         Specifies the size of the gap between rows
-- grid-row-start	         Specifies where to start the grid item
-- grid-template	         A shorthand property for the grid-template-rows, grid-template-columns and grid-areas properties
-- grid-template-areas	   Specifies how to display columns and rows, using named grid items
-- grid-template-columns	Specifies the size of the columns, and how many columns in a grid layout
-- grid-template-rows	   Specifies the size of the rows in a grid layout

```

autoFocus preventDefault preventScoll: true :: off auto scroll into view


